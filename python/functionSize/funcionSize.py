import os
import re

sourcePath = r'..\..\source'
lstFile = 'gr9Lab.lst'

inputFile = open(os.path.join(sourcePath, lstFile), 'r')
lines = inputFile.readlines()

patternStart = r'^\s+\d+\s([0-9A-F]{4})\s+\.local\s+(\w+)\s+;\s+(PROCEDURE|FUNCTION)'
patternEnd = r'^\s+\d+\s([0-9A-F]{4})\s+60\s+rts\s+; ret'
compStart = re.compile(patternStart)
compEnd = re.compile(patternEnd)


class Function:

    def __init__(self, name, start, end):
        self.name = name
        self.start = start
        self.end = end
        self.length = int(end, 16) - int(start, 16) + 1

    def __str__(self):
        return '{0} {1}'.format(self.name, self.length)


foundStart = False
foundEnd = False
functions = list()

for line in lines:
    mStart = compStart.match(line)
    mEnd = compEnd.match(line)
    if mStart:
        foundStart = True
        foundEnd = False
        funcName = mStart.group(2)
        startAddr = mStart.group(1)

    if mEnd:
        foundEnd = True
        if foundStart:
            endAddr = mEnd.group(1)
            functions.append(Function(funcName, startAddr, endAddr))
        foundStart = False
        foundEnd = False

functions = sorted(functions, key=lambda f: f.length, reverse=True)
for function in functions:
    print(function)
