import numpy as np
import cv2

levelRatio = 17
scaleRatio = 4


def correct_levels(img):
    return cv2.multiply(cv2.divide(img, levelRatio), levelRatio)


def downscale(img):
    height, width = img.shape[:2]
    return cv2.resize(img, (width // scaleRatio, height))


def upscale(img):
    height, width = img.shape[:2]
    return cv2.resize(img, (width * scaleRatio, height), interpolation=cv2.INTER_NEAREST)


def load_image(file_name):
    return correct_levels(downscale(cv2.imread(file_name, 0)))


def show_image(name, img):
    cv2.imshow(name, upscale(correct_levels(img)))


def encode_image(img):
    img = cv2.divide(img, levelRatio)
    height, width = img.shape[:2]
    half_width = width // 2
    result = np.empty((height, half_width), np.uint8)
    for y in range(height):
        for x in range(half_width):
            result[y, x] = img[y, 2 * x] * 16 + img[y, 2 * x + 1]
    return result


def decode_image(img):
    height, half_width = img.shape
    width = half_width * 2
    print(width)
    print(height)
    result = np.empty((height, width), np.uint8)
    for y in range(height):
        for x in range(half_width):
            result[y, 2 * x] = (img[y, x] // 16) * 16
            result[y, 2 * x + 1] = (img[y, x] % 16) * 16
    return result


def do_emboss(img):
    res = cv2.divide(img, levelRatio)
    emboss_kernel = np.array([[1, 0, -1]])
    res = cv2.filter2D(res, cv2.CV_16S, emboss_kernel)
    res += 7
    res = np.clip(res, 0, 255)
    res = res.astype('uint8')
    res = cv2.multiply(res, levelRatio)
    return res


def print_histogram(hist):
    for v in hist:
        print(int(v), end=' ')
    print('\n')


def calc_histogram(img):
    corrected = cv2.divide(img, levelRatio)
    hist = cv2.calcHist([corrected], [0], None, [256], [0, 256])
    hist = hist[0:16]

    print_histogram(hist)

    cum_hist = [0] * 16
    cum_hist[0] = hist[0]
    for i in range(1, 16):
        cum_hist[i] = cum_hist[i - 1] + hist[i]

    lut = [0] * 16
    for i in range(0, 16):
        lut[i] = (15 * int(cum_hist[i])) // (192 * 80)

    new_hist = [0] * 16
    for i in range(0, 16):
        new_hist[lut[i]] += hist[i]

    print_histogram(new_hist)


# original = load_image('einstein_320x192.png')
# encode_image(original).tofile('einstein_320x192.gr9')

original = load_image('lena_320x192.png')
encode_image(original).tofile('lena_320x192.gr9')
# encoded = np.fromfile('lena_320x192.dat', dtype=np.int8).reshape(192, 40)
# original = decode_image(encoded)

calc_histogram(original)

gaussian = correct_levels(cv2.GaussianBlur(original, (3, 3), 0))
kernel = np.array([[-0.125, -0.125, -0.125], [-.125, 2, -0.125], [-0.125, -0.125, -0.125]])
sharped = correct_levels(cv2.filter2D(original, -1, kernel))
negative = correct_levels(cv2.bitwise_not(original))
kernel = np.array([[-0.125, -0.125, -0.125], [-0.125, 1, -0.125], [-0.125, -0.125, -0.125]])
edges = correct_levels(cv2.filter2D(original, -1, kernel))
kernel = np.array([[1 / 16, 2 / 16, 3 / 16, 4 / 16, 3 / 16, 2 / 16, 1 / 16]])
motion = correct_levels(cv2.filter2D(original, -1, kernel))

emboss = correct_levels(do_emboss(original))

show_image('original', original)
show_image('gaussian', gaussian)
show_image('sharped', sharped)
show_image('negative', negative)
show_image('edges', edges)
show_image('motion', motion)
show_image('emboss', emboss)

cv2.waitKey()
cv2.destroyAllWindows()
