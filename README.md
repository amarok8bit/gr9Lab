# gr9Lab

## General information ##

gr9Lab is an application designed for image processing on 8-bit Atari computer.

It is possible to work with two images in GR9 mode with resolution of 80x192 pixels at the same time.
The program has ability to process the whole images or within a selected rectangular area of interest (ROI).

The program can be controlled by joystick, Atari Trakball, Atari mouse, Amiga mouse or the keyboard:
- by pressing 1 and 2 keys you can switch the view between two images.
- by pressing Tab/Shift Tab keys you can go to the next/previous menu item or other GUI control.
- by pressing Return key you can activate highlighted menu item or other GUI control.
- by pressing Esc key you can go to the main menu.

## Videos ###

gr9Lab - image processing on Atari 8-bit computer (WIP demo 1):

[![gr9Lab - image processing on Atari 8-bit computer (WIP demo 1)](https://img.youtube.com/vi/Vf-eK0A_iIE/0.jpg)](https://www.youtube.com/watch?v=Vf-eK0A_iIE)

gr9Lab (WIP demo 2) - mouse support:

[![gr9Lab - image processing on Atari 8-bit computer (WIP demo 1)](https://img.youtube.com/vi/rkCook3_KZ4/0.jpg)](https://www.youtube.com/watch?v=rkCook3_KZ4)

gr9Lab (WIP demo 2) - benchmark:

[![gr9Lab - image processing on Atari 8-bit computer (WIP demo 1)](https://img.youtube.com/vi/wx7DLL6ZfNY/0.jpg)](https://www.youtube.com/watch?v=wx7DLL6ZfNY)

## List of functions ##

The list of features available in the current version contains as follows.

**gr9Lab menu**
- New - clears content of the image.
- Load - loads image from a file in GR9 format (80x192 pixels).
- Save - saves image to file in GR9 format (80x192 pixels).
- Exit - closes the application.
- Copy - copies image content from the other one.
- Show/Hide selection - turns on/off selection (ROI).
- Settings - settings window to choose controller type and turn on/off fast processing.

**Process menu**
- Flip horizontal - performs mirror of the image in horizontal direction.
- Flip vertical - performs mirror of the image in vertical direction.
- Rotate 180 - rotates the image by 180 degrees.
- Brightness/Contrast - adjusts brightness and contrast of the image.
- Histogram - calculates and shows a histogram of the image.
- Histogram equalize - performs histogram equalizaion on the image.
- Inverse - applies inversion of pixels intensity.
- Threshold - performs thresholding operation, pixels replaced by black if are darker than parameter or replaced by white otherwise.
- Add noise - applies random modification of pixels intensity.

**Filter menu**
- Box filter - applies average filter of 3x3 pixels on the image.
- Gaussian blur - applies Gaussian blur filter of 3x3 pixels on the image.
- Sharpen - performes sharpening operation using filter of 3x3 pixels.
- Laplace edges - enhances edges on the image by Laplace filter of 3x3 pixels.
- Emboss - generates relief effect on the image by using filter of 3x3 pixels.
- Dilate filter - applies morphological dilate filter of 3x3 pixels on the image.
- Erode filter - applies morphological erode filter of 3x3 pixels on the image.
- Opening filter - applies morphological opening filter of 3x3 pixels on the image.
- Closing filter - applies morphological closing filter of 3x3 pixels on the image.

**Math menu**
- Set - fill the image by a selected intensity.
- Add - increases pixels intensity by selected value.
- Subtract - decrease pixels intensity by selected value.
- Difference - calculates absolute difference between pixels intensity and selected value.
- Multiply - multiplies pixels intensity by selected value.
- Divide - divides pixels intensity by selected value.
- Log - calculates logarithm values from pixels intensity.
- Exp - calculates exponent values from pixels intensity.
- Sqr - calculates power of 2 from pixels intensity.
- Sqrt - calculates square root from pixels intensity.
- Not - calculates bitwise negation of pixels intensity.
- Or - calculates bitwise sum of pixels intensity and selected value.
- And - calculates bitwise product of pixels intensity and selected value.
- Xor - calculates bitwise exclusive disjunction of pixels intensity and selected value.
- Minimum - calulates minimum value from pixels intensity and selected value.
- Maximum - calulates maximum value from pixels intensity and selected value.

**Blender menu**
- Add - calculates addition between corresponding pixels of two images.
- Subtract - calculates subraction between corresponding pixels of two images.
- Difference - calculates absolute difference between corresponding pixels of two images.
- Multiply - calculates multiplication between corresponding pixels of two images.
- Divide - calculates division between corresponding pixels of two images.
- Average - calculates average value between corresponding pixels of two images.
- Or - calculates bitwise sum between corresponding pixels of two images.
- And - calculates bitwise product between corresponding pixels of two images.
- Xor - calculates bitwise exclusive disjunction between corresponding pixels of two images.
- Minimum - calculates minimum from corresponding pixels of two images.
- Maximum - calculates maximum from corresponding pixels of two images.
