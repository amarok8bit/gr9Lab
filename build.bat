echo off

@setlocal
@set MP_PATH=D:\Ulubione\Atari\mp_163_20200927\
@set FILE_NAME=gr9Lab
@set PATH=%PATH%;%MP_PATH%;D:\Ulubione\Atari\Altirra-3.90\

@set PAS_NAME=source\%FILE_NAME%.pas
@set A65_NAME=source\%FILE_NAME%.a65
@set XEX_NAME=%FILE_NAME%.xex
@set ATR_NAME=%FILE_NAME%.atr

del %A65_NAME%
del %XEX_NAME%
del disk\AUTORUN.SYS
del %FILE_NAME%.atr

mp.exe %PAS_NAME% -o -ipath:source -data:8000 -code:8A00
if not exist %A65_NAME% goto :exit

mads.exe %A65_NAME% -x -i:%MP_PATH%base -o:%XEX_NAME%
if not exist %XEX_NAME% goto :exit

copy /y %XEX_NAME% disk\AUTORUN.SYS
tools\dir2atr.exe -S -b Dos25 %ATR_NAME% disk

Altirra64.exe %ATR_NAME%

:exit