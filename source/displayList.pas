unit displayList;

interface

var
  topLinesCount: Byte;

procedure FillDisplayList(screenPtr: Pointer);
procedure RollDownImage(screenPtr: Pointer);
procedure RollUpImage(screenPtr: Pointer);

implementation

uses crt, imgCore, memoryMap;

const
  LOCAL_IMAGE_STEP = IMAGE_STEP;
  LOCAL_IMAGE_HEIGHT = IMAGE_HEIGHT;
  LOCAL_DL_ADDR_1 = DL_ADDR_1;
  LOCAL_DL_ADDR_2 = DL_ADDR_2;
  LOCAL_TEXT_ADDR = TEXT_ADDR;
  LOCAL_STATUS_ADDR = STATUS_ADDR;

  ANTIC_LMS = $40;
  ANTIC_DLI = $80;
  ANTIC_EMPTY_1 = $00;
  ANTIC_EMPTY_4 = $30;
  ANTIC_EMPTY_7 = $60;
  ANTIC_EMPTY_8 = $70;
  ANTIC_EMPTY_1_DLI = ANTIC_EMPTY_1 or ANTIC_DLI;
  ANTIC_MODE_2 = $02;
  ANTIC_MODE_F = $0f;
  ANTIC_MODE_2_LMS = ANTIC_MODE_2 or ANTIC_LMS;
  ANTIC_MODE_F_LMS = ANTIC_MODE_F or ANTIC_LMS;
  //ANTIC_MODE_F_LMS_DLI = $CF;//ANTIC_MODE_F_LMS or ANTIC_DLI;
  ANTIC_JVB = $41;


var
  firstDlAddr: Boolean;
  imageOffset: Byte;
  imageHeight: Byte;

// http://www.atari.org.pl/artykul/kurs-assemblera-cz.-4/27
procedure InternalFillDisplayList(screenPtr: Pointer);
var
  line: Byte;
  block, oldBlock: Byte;
  dl: Word absolute $e0;
  screenAddr: Word;
  i: Byte;
  startDlAddr: Word;
begin
asm
{
  phr
  
  lda firstDlAddr
  jeq set_dl_addr_1
  mva #.lo(LOCAL_DL_ADDR_2) startDlAddr
  mva #.hi(LOCAL_DL_ADDR_2) startDlAddr+1
  jmp set_dl
    
set_dl_addr_1

  mva #.lo(LOCAL_DL_ADDR_1) startDlAddr
  mva #.hi(LOCAL_DL_ADDR_1) startDlAddr+1
  
set_dl

  mva startDlAddr dl
  mva startDlAddr+1 dl+1
  ldy #0

  ldx imageOffset

offset_loop

  beq top_part
  mva #ANTIC_EMPTY_4 (dl),y
  iny
  dex
  jmp offset_loop

top_part

  mva #ANTIC_EMPTY_8 (dl),y
  iny
  mva #ANTIC_EMPTY_7 (dl),y
  iny  
  mva #ANTIC_MODE_2_LMS (dl),y
  iny
  mva #.lo(LOCAL_TEXT_ADDR) (dl),y
  iny
  mva #.hi(LOCAL_TEXT_ADDR) (dl),y
  iny
  
  ldx topLinesCount

top_loop
  
  dex
  beq end_top_loop
  mva #ANTIC_MODE_2 (dl),y
  iny
  jmp top_loop
  
end_top_loop
 
  mva #ANTIC_EMPTY_1_DLI (dl),y
  iny

  ;line := 8 * (topLinesCount - 1); 
  ldx topLinesCount
  dex
  txa
  asl
  asl
  asl
  sta line

  ;screenAddr := Word(screenPtr) + line * IMAGE_STEP; 
  sta :eax
  mva #LOCAL_IMAGE_STEP :ecx
  
  sty i
  imulCL
  ldy i
  
  lda screenPtr
  add :eax
  sta screenAddr
  lda screenPtr+1
  adc :eax+1
  sta screenAddr+1

  mva #0 oldBlock
  ldx line
     
loop
  
  ;while line < imageHeight do
  cpx imageHeight
  jcs end_loop

  ;block := Hi(screenAddr) shr 4;
  lda screenAddr+1
  lsr
  lsr
  lsr
  lsr
  sta block
   
  ;if block <> oldBlock then
  cmp oldBlock
  beq equal
  
  mva block oldBlock
  mva #ANTIC_MODE_F_LMS (dl),y
  iny
  mva screenAddr (dl),y
  iny
  mva screenAddr+1 (dl),y
  jmp next_line
  
equal
 
  mva #ANTIC_MODE_F (dl),y

next_line

  iny
  inx
  
  ;Inc(screenAddr, IMAGE_STEP);
  lda screenAddr
  add #LOCAL_IMAGE_STEP
  sta screenAddr
  scc
  inc screenAddr+1
   
  jmp loop
  
end_loop

  mva #ANTIC_EMPTY_1_DLI (dl),y
  iny
  mva #ANTIC_MODE_2_LMS (dl),y
  iny
  mva #.lo(LOCAL_STATUS_ADDR) (dl),y
  iny
  mva #.hi(LOCAL_STATUS_ADDR) (dl),y
  iny
  mva #ANTIC_JVB (dl),y
  iny
  mva startDlAddr (dl),y
  iny
  mva startDlAddr+1 (dl),y

  plr
  };
  
  if firstDlAddr then 
  begin
	  SDLSTL := Word(DL_ADDR_2)
  end
  else begin
	  SDLSTL := Word(DL_ADDR_1)
  end;
  
  firstDlAddr := not firstDlAddr;
end;

procedure FillDisplayList(screenPtr: Pointer);
begin
  imageOffset := 0;
  imageHeight := IMAGE_HEIGHT;
  InternalFillDisplayList(screenPtr);
end;

procedure RollDownImage(screenPtr: Pointer);
const
  START_IMAGE_OFFSET = IMAGE_HEIGHT div 8;
begin
  imageOffset := START_IMAGE_OFFSET;
  imageHeight := 8;
  repeat
    InternalFillDisplayList(screenPtr);
    Delay(20);
    Inc(imageHeight, 8);
    Dec(imageOffset);
  until imageHeight = IMAGE_HEIGHT;
end;

procedure RollUpImage(screenPtr: Pointer);
begin
  imageOffset := 0;
  imageHeight := IMAGE_HEIGHT;
  repeat
    InternalFillDisplayList(screenPtr);
    Delay(20);
    Dec(imageHeight, 8);
    Inc(imageOffset);
  until imageHeight = 8;
end;

initialization
  firstDlAddr := False;
end.