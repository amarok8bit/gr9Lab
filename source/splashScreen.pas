unit splashScreen;

interface

procedure ShowSplashScreen;

implementation

uses
  crt, memoryMap;

const
  CHBAS = $02F4;
  DEF_CHARSET = $E0;

var
  charsetValue: Byte;
  logoColor: Byte;

procedure SplashDli; interrupt; assembler;
var
  CHBASE: Byte absolute $D409;
const
  LOGO_CHARSET = Hi(PM_P0_ADDR);
asm
{
  pha

  mva logoColor COLPF1

  lda charsetValue
  cmp #DEF_CHARSET
  beq equal_E0
     
  lda #DEF_CHARSET
  jmp set_charset

equal_E0
  lda #LOGO_CHARSET

set_charset
  sta charsetValue
  sta CHBASE
  pla
};
end;

procedure SplashVbl; interrupt; assembler;
asm
{
  mva #DEF_CHARSET charsetValue
  jmp xitvbv
};
end;

procedure ShowSplashScreen;
var
  oldVbl: Pointer;
  oldDli: Pointer;
  oldSDMCTL: Byte;
  oldSDLSTL: Byte;
  i: Byte;
const
  BACKGROUND_COLOR = $00;
  TEXT_COLOR = $0F;
  DELAY_VALUE = 30;

begin
  COLOR1 := BACKGROUND_COLOR;
  COLOR2 := BACKGROUND_COLOR;
  COLOR4 := BACKGROUND_COLOR;
  logoColor := BACKGROUND_COLOR;

  oldSDMCTL := SDMCTL;
  SDMCTL := 0;

  oldVbl := nil;
  GetIntVec(iVbl, oldVbl);
  SetIntVec(iVbl, @SplashVbl);

  oldDli := nil;
  GetIntVec(iDLI, oldDli);
  SetIntVec(iDli, @SplashDli);
  NMIEN := $C0;

  oldSDLSTL := SDLSTL;
  SDLSTL := Word(DL_ADDR_2);

  SDMCTL := oldSDMCTL;

  while COLOR1 < TEXT_COLOR do
  begin
    Delay(DELAY_VALUE);
    Inc(COLOR1);
  end;
  while logoColor < TEXT_COLOR do
  begin
    Delay(DELAY_VALUE);
    Inc(logoColor);
  end;

  i := 0;
  while i < 20 do
  begin
    if Keypressed then
    begin
      ReadKey;
      break;
    end;
    Delay(100);
    Inc(i);
  end;

  while COLOR1 > BACKGROUND_COLOR do
  begin
    Delay(DELAY_VALUE);
    Dec(COLOR1);
  end;
  while logoColor > BACKGROUND_COLOR do
  begin
    Delay(DELAY_VALUE);
    Dec(logoColor);
  end;

  SDMCTL := 0;
  Delay(100);
  SDLSTL := oldSDLSTL;
  NMIEN := 0;
  SetIntVec(iDli, oldDli);
  SetIntVec(iVbl, oldVbl);

  SDMCTL := oldSDMCTL;
end;

end.