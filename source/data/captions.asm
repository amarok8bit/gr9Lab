CAPTIONS_LIST

    dta a(cap_main_menu)
    dta a(cap_gr9lab)
    dta a(cap_image)
    dta a(cap_process)
    dta a(cap_filter)
    dta a(cap_math)
    dta a(cap_blender)
    dta a(cap_new)
    dta a(cap_load)
    dta a(cap_save)
    dta a(cap_exit)
    dta a(cap_copy)
    dta a(cap_show_hide_selection)
    dta a(cap_flip_horizontal)
    dta a(cap_flip_vertical)
    dta a(cap_rotate_180)
    dta a(cap_brightness_contrast)
    dta a(cap_inverse)
    dta a(cap_threshold)
    dta a(cap_box_filter)
    dta a(cap_gaussian_blur)
    dta a(cap_sharpen)
    dta a(cap_laplace_edges)
    dta a(cap_emboss)
    dta a(cap_dilate_filter)
    dta a(cap_erode_filter)
    dta a(cap_opening_filter)
    dta a(cap_closing_filter)
    dta a(cap_add)
    dta a(cap_subtract)
    dta a(cap_difference)
    dta a(cap_multiply)
    dta a(cap_divide)
    dta a(cap_average)
    dta a(cap_or)
    dta a(cap_and)
    dta a(cap_xor)
    dta a(cap_minimum)
    dta a(cap_maximum)
    dta a(cap_yes)
    dta a(cap_no)
    dta a(cap_decrease)
    dta a(cap_increase)
    dta a(cap_ok)
    dta a(cap_cancel)
    dta a(cap_enter_file_path)
    dta a(cap_io_error)
    dta a(cap_io_error_occurred)
    dta a(cap_histogram)
    dta a(cap_add_noise)
    dta a(cap_value)
    dta a(cap_log)
    dta a(cap_exp)
    dta a(cap_sqr)
    dta a(cap_sqrt)
    dta a(cap_not)
    dta a(cap_set)
    dta a(cap_histogram_equalization)
    dta a(cap_settings)
    dta a(cap_controller)
    dta a(cap_joystick)
    dta a(cap_atari_mouse)
    dta a(cap_amiga_mouse)
    dta a(cap_trakball)
    dta a(cap_controller_port)
    dta a(cap_one)
    dta a(cap_two)
    dta a(cap_fast_processing)
    dta a(cap_on)
    dta a(cap_off)

CAPTIONS_LENGTH

    dta 9   ;Main menu
    dta 6   ;gr9Lab
    dta 5   ;Image
    dta 7   ;Process
    dta 6   ;Filter
    dta 4   ;Math
    dta 7   ;Blender
    dta 3   ;New
    dta 4   ;Load
    dta 4   ;Save
    dta 4   ;Exit
    dta 4   ;Copy
    dta 19  ;Show/Hide selection
    dta 15  ;Flip horizontal
    dta 13  ;Flip vertical
    dta 10  ;Rotate 180
    dta 19  ;Brightness/Contrast
    dta 7   ;Inverse
    dta 9   ;Threshold
    dta 10  ;Box filter
    dta 13  ;Gaussian blur
    dta 7   ;Sharpen
    dta 13  ;Laplace edges
    dta 6   ;Emboss
    dta 13  ;Dilate filter
    dta 12  ;Erode filter
    dta 14  ;Opening filter
    dta 14  ;Closing filter
    dta 3   ;Add
    dta 8   ;Subtract
    dta 10  ;Difference
    dta 8   ;Multiply
    dta 6   ;Divide
    dta 7   ;Average
    dta 2   ;Or
    dta 3   ;And
    dta 3   ;Xor
    dta 7   ;Minimum
    dta 7   ;Maximum
    dta 3   ;Yes
    dta 2   ;No
    dta 1   ;-
    dta 1   ;+
    dta 2   ;Ok
    dta 6   ;Cancel
    dta 16  ;Enter file path:
    dta 9   ;I/O Error
    dta 20  ;I/O Error occurred #
    dta 9   ;Histogram
    dta 9   ;Add noise
    dta 5   ;Value
    dta 3   ;Log
    dta 3   ;Exp
    dta 3   ;Sqr
    dta 4   ;Sqrt
    dta 3   ;Not
    dta 3   ;Set
    dta 18  ;Histogram equalize
    dta 8   ;Settings
    dta 10  ;Controller
    dta 8   ;Joystick
    dta 11  ;Atari mouse
    dta 11  ;Amiga mouse
    dta 8   ;Trakball
    dta 15  ;Controller port
    dta 3   ;One
    dta 3   ;Two
    dta 15  ;Fast processing
    dta 2   ;On
    dta 3   ;Off

CAPTIONS

cap_main_menu               dta d'Main menu'
cap_gr9lab                  dta d'gr9Lab'
cap_image                   dta d'Image'
cap_process                 dta d'Process'
cap_filter                  dta d'Filter'
cap_math                    dta d'Math'
cap_blender                 dta d'Blender'
cap_new                     dta d'New'
cap_load                    dta d'Load'
cap_save                    dta d'Save'
cap_exit                    dta d'Exit'
cap_copy                    dta d'Copy'
cap_show_hide_selection     dta d'Show/Hide selection'
cap_flip_horizontal         dta d'Flip horizontal'
cap_flip_vertical           dta d'Flip vertical'
cap_rotate_180              dta d'Rotate 180'
cap_brightness_contrast     dta d'Brightness/Contrast'
cap_inverse                 dta d'Inverse'
cap_threshold               dta d'Threshold'
cap_box_filter              dta d'Box filter'
cap_gaussian_blur           dta d'Gaussian blur'
cap_sharpen                 dta d'Sharpen'
cap_laplace_edges           dta d'Laplace edges'
cap_emboss                  dta d'Emboss'
cap_dilate_filter           dta d'Dilate filter'
cap_erode_filter            dta d'Erode filter'
cap_opening_filter          dta d'Opening filter'
cap_closing_filter          dta d'Closing filter'
cap_add                     dta d'Add'
cap_subtract                dta d'Subtract'
cap_difference              dta d'Difference'
cap_multiply                dta d'Multiply'
cap_divide                  dta d'Divide'
cap_average                 dta d'Average'
cap_or                      dta d'Or'
cap_and                     dta d'And'
cap_xor                     dta d'Xor'
cap_minimum                 dta d'Minimum'
cap_maximum                 dta d'Maximum'
cap_yes                     dta d'Yes'
cap_no                      dta d'No'
cap_decrease                dta d'-'
cap_increase                dta d'+'
cap_ok                      dta d'Ok'
cap_cancel                  dta d'Cancel'
cap_enter_file_path         dta d'Enter file path:'
cap_io_error                dta d'I/O Error'
cap_io_error_occurred       dta d'I/O Error occurred #'
cap_histogram               dta d'Histogram'
cap_add_noise               dta d'Add noise'
cap_value                   dta d'Value'
cap_log                     dta d'Log'
cap_exp                     dta d'Exp'
cap_sqr                     dta d'Sqr'
cap_sqrt                    dta d'Sqrt'
cap_not                     dta d'Not'
cap_set                     dta d'Set'
cap_histogram_equalization  dta d'Histogram equalize'
cap_settings                dta d'Settings'
cap_controller              dta d'Controller'
cap_joystick                dta d'Joystick'
cap_atari_mouse             dta d'Atari mouse'
cap_amiga_mouse             dta d'Amiga mouse'
cap_trakball                dta d'Trakball'
cap_controller_port         dta d'Controller port'
cap_one                     dta d'One'
cap_two                     dta d'Two'
cap_fast_processing         dta d'Fast processing'
cap_on                      dta d'On'
cap_off                     dta d'Off' 