unit imgCore;

interface

const
{$I 'common.inc'}

type
  PByte = ^Byte;

  TFunctionKind = (
    fkCopyImage,
    fkInverse,
    fkFlipHorizontal,
    fkFlipVertical,
    fkRotate180,
    fkLut,
    fkBoxFilter,
    fkGaussFilter,
    fkSharpenFilter,
    fkLaplaceFilter,
    fkMaxFilter,
    fkMinFilter,
    fkOpeningFilter,
    fkClosingFilter,
    fkEmbossFilter,
    fkDrawRoi,
    fkSaveRoiBuffers,
    fkRestoreRoiBuffers,
    fkCalcHistogram,
    fkAddNoise,
    fkBrightnessContrast,
    fkHistogramEqualization,
    fkAddValue,
    fkSubtractValue,
    fkDifferenceFromValue,
    fkMultiplyByValue,
    fkDivideByValue,
    fkOrWithValue,
    fkAndWithValue,
    fkXorWithValue,
    fkMinimumWithValue,
    fkMaximumWithValue,
    fkThreshold,
    fkLog,
    fkExp,
    fkSqr,
    fkSqrt,
    fkImageBlender,
    fkSet
  );

  TImageBlenderOperation = (
    iboAdd,
    iboSubtract,
    iboDifference,
    iboMultiply,
    iboDivide,
    iboAverage,
    iboOr,
    iboAnd,
    iboXor,
    iboMinimum,
    iboMaximum);

const
  MAX_LUMINANCE_MINUS_1 = MAX_LUMINANCE - 1;

var
  functionKind: Byte absolute FUNCTION_KIND_ADDR;

  srcPtr: Word absolute SRC_PTR_ADDR;
  src2Ptr: Word absolute SRC2_PTR_ADDR;
  dstPtr: Word absolute DST_PTR_ADDR;
  LUT: array[0..MAX_LUT] of Byte absolute LUT_ARRAY_ADDR;
  HistogramHi: array[0..MAX_LUMINANCE] of Byte absolute HISTOGRAM_HI_ARRAY_ADDR;
  HistogramLo: array[0..MAX_LUMINANCE] of Byte absolute HISTOGRAM_LO_ARRAY_ADDR;
  HistogramNorm: array[0..MAX_LUMINANCE] of Byte absolute HISTOGRAM_NORM_ARRAY_ADDR;
  
  roiActive: Boolean absolute ROI_ACTIVE_ADDR;
  roiLeft, roiTop: Byte;
  roiRight, roiBottom: Byte;
  roiWidth: Byte absolute ROI_WIDTH_ADDR;
  roiHeight: Byte absolute ROI_HEIGHT_ADDR;
  roiHeightPlusOne: Byte absolute ROI_HEIGHT_PLUS_ONE_ADDR;
  roiWidthBytes: Byte absolute ROI_WIDTH_BYTES_ADDR;
  roiWidthBytesMinusOne: Byte absolute ROI_WIDTH_BYTES_MINUS_ONE_ADDR;
  roiLeftMask: Byte absolute ROI_LEFT_MASK_ADDR;
  roiRightMask: Byte absolute ROI_RIGHT_MASK_ADDR;
  roiLeftBackup, roiTopBackup, roiWidthBackup, roiHeightBackup: Byte;

  imageBlenderOperation: Byte absolute IMAGE_BLENDER_OPERATION_ADDR;

  intensityVal: Byte absolute INTENSITY_VALUE_ADDR; // 0 .. 15
  brightness: Byte absolute BRIGHTNESS_VALUE_ADDR; // 1 (-15) .. 16 (0) .. 31 (15)
  contrast: Byte absolute CONTRAST_VALUE_ADDR; // 1 (-15) .. 16 (0) .. 31 (15)

implementation
end.