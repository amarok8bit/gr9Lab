unit caption;

interface

uses memoryMap;

type
  TCaption = (
    capMainMenu,
    capGr9Lab, capImage, capProcess, capFilter, capMath, capBlender,
    capNew, capLoad, capSave, capExit,
    capCopy, capShowHideSelection,
    capFlipHorizontal, capFlipVertical, capRotate180, capBrightnessContrast, capInverse, capThreshold,
    capBoxFilter, capGaussianBlur, capSharpen, capLaplaceEdges, capEmboss, capDilateFilter, capErodeFilter, capOpeningFilter, capClosingFilter,
    capAdd, capSubtract, capDifference, capMultiply, capDivide, capAverage, capOr, capAnd, capXor, capMinimum, capMaximum,
    capYes, capNo,
    capDec, capInc, capOk, capCancel,
    capEnterFilePath, capIOError, capIOErrorOccurred,
    capHistogram,
    capAddNoise,
    capValue,
    capLog, capExp, capSqr, capSqrt, capNot, capSet,
    capHistogramEqualization,
    capSettings, capController, capJoystick, capAtariMouse, capAmigaMouse, capTrakball,
    capControllerPort, capOne, capTwo, capFastProcessing, capOn, capOff);
    
var
  CAPTIONS: array[0..CAPTIONS_COUNT - 1] of Word absolute CAPTIONS_ADDR;
  CAPTIONS_LENGTHS: array[0..CAPTIONS_COUNT - 1] of Byte absolute CAPTIONS_LENGTHS_ADDR;

implementation
end.