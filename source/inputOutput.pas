unit inputOutput;

interface

type
  TLoadSaveAction = (
    lsaLoad, lsaSave);

var
  loadSaveAction: TLoadSaveAction;
  filePath: TString;
  lastIOResult: Byte;

procedure LoadSaveImage;

implementation

uses imgCore;

procedure LoadSaveImage;
var
  f: File;
  buf: PByte;
const
  BLOCK_COUNT = IMAGE_SIZE div 128;
begin
  {$I-}
  Assign(f, filePath);
  if loadSaveAction = lsaLoad then
  begin
    Reset(f);
  end
  else begin
    Rewrite(f);
  end;

  if IOResult >= 128 then
  begin
    lastIOResult := IOResult;
  end
  else begin
    buf := Pointer(dstPtr);
    if loadSaveAction = lsaLoad then
    begin
      BlockRead(f, buf, BLOCK_COUNT);
    end
    else begin
      BlockWrite(f, buf, BLOCK_COUNT);
    end;
    lastIOResult := IOResult;
  end;
  Close(f);
  {$I+}
end;

end.