unit utils;

interface

var
  byteValue: Byte;
  textBufferPos: Byte;
  
procedure PutByteToTextBuffer;
function IntToAnticString(value: Integer): string;

implementation

uses memoryMap, sysUtils;

procedure PutByteToTextBuffer;
var
  tmp: Byte;
  buffer: array[0..39] of Byte absolute TEXT_BUFFER_ADDR;
begin
  if byteValue >= 10 then Inc(textBufferPos);
  if byteValue >= 100 then Inc(textBufferPos);
  
  repeat
    tmp := byteValue mod 10;
    Inc(tmp, Ord('0'~));
    buffer[textBufferPos] := tmp;
    Dec(textBufferPos);
    byteValue := byteValue div 10;
  until byteValue = 0;  
end;

function IntToAnticString(value: Integer): string;
var
  i, len: Byte;
  ptr: PByte;
begin
  Result := IntToStr(value);
  len := Byte(Result[0]);
  ptr := @Result[1];
  for i := 1 to len do
  begin
    case (ptr^ and $7f) of
      0..31: Inc(ptr^, 64);
      32..95: Dec(ptr^, 32);
    end;
    Inc(ptr);
  end;
end;

end.