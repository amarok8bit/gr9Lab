unit menu;

interface

uses caption, action, memoryMap, cursor, keyboard, settings;

type
  TMenuKind = (
    mkMainMenu,
    mkHome,
    mkProcess,
    mkFilter,
    mkMath,
    mkBlender,
    mkNewQuery,
    mkCloseQuery,
    mkIntensity,
    mkBrightnessContrast,
    mkLoadSave,
    mkLoadSaveError,
    mkHistogram,
    mkSettings);

const
  MENU_COUNT: Byte = 14;
  MAX_MENU_ITEM_COUNT: Byte = 16;
  MENU_HEIGHT: array[0..MENU_COUNT - 1] of Byte = (
    1, 5, 6, 6, 7, 5, 4, 4, 6, 7, 7, 6, 10, 8);

var
  menuKind: TMenuKind;
  menuItemCount: Byte;
  menuItemIndex: Byte;
  newMenuItemIndex: Byte;
  intensityCaption: TCaption;
  menuFastProcessing: Boolean;
  menuControllerKind: TControllerKind;
  menuControllerPortTwo: Boolean;

  menuPosPosSuccess: Boolean;
  menuItems: array[0..MAX_MENU_ITEM_COUNT-1] of Byte;

procedure UpdateMenu;
procedure UpdateMenuItemIndex;
procedure GetMenuPos;
procedure EditFilePath;
procedure IntensityDec;
procedure IntensityInc;
procedure BrightnessDec;
procedure BrightnessInc;
procedure ContrastDec;
procedure ContrastInc;
procedure CopyMenuItems;

implementation

uses utils, imgCore, inputOutput;

const
  HORIZONTAL_LINE: Byte = 82;

  MAIN_MENU_ITEM_COUNT = 5;
  HOME_MENU_ITEM_COUNT = 7;
  IMAGE_MENU_ITEM_COUNT = 9;
  FILTER_MENU_ITEM_COUNT = 9;
  MATH_MENU_ITEM_COUNT = 16;
  BLENDER_MENU_ITEM_COUNT = 11;
  NEW_MENU_ITEM_COUNT = 2;
  EXIT_MENU_ITEM_COUNT = 2;
  INTENSITY_MENU_ITEM_COUNT = 4;
  BRIGHTNESS_CONTRAST_MENU_ITEM_COUNT = 6;
  LOAD_SAVE_MENU_ITEM_COUNT = 2;
  LOAD_SAVE_ERROR_MENU_ITEM_COUNT = 1;
  HISTOGRAM_MENU_ITEM_COUNT = 1;
  SETTINGS_MENU_ITEM_COUNT = 4;

  MAIN_MENU_FIRST_ITEM = 0;
  HOME_MENU_FIRST_ITEM = MAIN_MENU_FIRST_ITEM + MAIN_MENU_ITEM_COUNT;
  IMAGE_MENU_FIRST_ITEM = HOME_MENU_FIRST_ITEM + HOME_MENU_ITEM_COUNT;
  FILTER_MENU_FIRST_ITEM = IMAGE_MENU_FIRST_ITEM + IMAGE_MENU_ITEM_COUNT;
  MATH_MENU_FIRST_ITEM = FILTER_MENU_FIRST_ITEM + FILTER_MENU_ITEM_COUNT;
  BLENDER_MENU_FIRST_ITEM = MATH_MENU_FIRST_ITEM + MATH_MENU_ITEM_COUNT;
  NEW_MENU_FIRST_ITEM = BLENDER_MENU_FIRST_ITEM + BLENDER_MENU_ITEM_COUNT;
  EXIT_MENU_FIRST_ITEM = NEW_MENU_FIRST_ITEM + NEW_MENU_ITEM_COUNT;
  INTENSITY_MENU_FIRST_ITEM = EXIT_MENU_FIRST_ITEM + EXIT_MENU_ITEM_COUNT;
  BRIGHTNESS_CONTRAST_MENU_FIRST_ITEM = INTENSITY_MENU_FIRST_ITEM + INTENSITY_MENU_ITEM_COUNT;
  LOAD_SAVE_MENU_FIRST_ITEM = BRIGHTNESS_CONTRAST_MENU_FIRST_ITEM + BRIGHTNESS_CONTRAST_MENU_ITEM_COUNT;
  LOAD_SAVE_ERROR_MENU_FIRST_ITEM = LOAD_SAVE_MENU_FIRST_ITEM + LOAD_SAVE_MENU_ITEM_COUNT;
  HISTOGRAM_MENU_FIRST_ITEM = LOAD_SAVE_ERROR_MENU_FIRST_ITEM + LOAD_SAVE_ERROR_MENU_ITEM_COUNT;
  SETTINGS_MENU_FIRST_ITEM = HISTOGRAM_MENU_FIRST_ITEM + HISTOGRAM_MENU_ITEM_COUNT;
    
  MENU_CAPTIONS: array[0..MENU_COUNT - 1] of Byte = (
    capMainMenu, capGr9Lab, capProcess, capFilter, capMath, capBlender, capNew, capExit, capThreshold,
    capBrightnessContrast, capLoad, capIOError, capHistogram, capSettings);
  MENU_ITEM_COUNT: array[0..MENU_COUNT - 1] of Byte = (
    MAIN_MENU_ITEM_COUNT, HOME_MENU_ITEM_COUNT, IMAGE_MENU_ITEM_COUNT,
    FILTER_MENU_ITEM_COUNT, MATH_MENU_ITEM_COUNT, BLENDER_MENU_ITEM_COUNT, NEW_MENU_ITEM_COUNT, EXIT_MENU_ITEM_COUNT,
    INTENSITY_MENU_ITEM_COUNT, BRIGHTNESS_CONTRAST_MENU_ITEM_COUNT, LOAD_SAVE_MENU_ITEM_COUNT,
    LOAD_SAVE_ERROR_MENU_ITEM_COUNT, HISTOGRAM_MENU_ITEM_COUNT, SETTINGS_MENU_ITEM_COUNT);
  MENU_ITEMS_FIRST: array[0..MENU_COUNT - 1] of Byte = (
    MAIN_MENU_FIRST_ITEM, HOME_MENU_FIRST_ITEM, IMAGE_MENU_FIRST_ITEM,
    FILTER_MENU_FIRST_ITEM, MATH_MENU_FIRST_ITEM, BLENDER_MENU_FIRST_ITEM, NEW_MENU_FIRST_ITEM, EXIT_MENU_FIRST_ITEM,
    INTENSITY_MENU_FIRST_ITEM, BRIGHTNESS_CONTRAST_MENU_FIRST_ITEM, LOAD_SAVE_MENU_FIRST_ITEM,
    LOAD_SAVE_ERROR_MENU_FIRST_ITEM, HISTOGRAM_MENU_FIRST_ITEM, SETTINGS_MENU_FIRST_ITEM);

  MENU_ITEM_X: array[0..ACTION_COUNT - 1] of Byte = (
    1, 9, 18, 26, 32,
    1, 1, 1, 1, 10, 10, 10,
    1, 20, 1, 20, 1, 20, 1, 20, 1,
    1, 22, 1, 22, 1, 22, 1, 22, 1,
    1, 14, 28, 1, 14, 28, 1, 14, 28, 1, 14, 28, 1, 14, 28, 1,
    1, 14, 28, 1, 14, 28, 1, 14, 28, 1, 14,
    15, 22,
    15, 22,
    25, 28, 15, 19,
    25, 28, 25, 28, 15, 19,
    15, 19,
    18,
    35,
    22, 22, 22, 19);

  MENU_ITEM_Y: array[0..ACTION_COUNT - 1] of Byte = (
    0, 0, 0, 0, 0,
    1, 2, 3, 4, 1, 2, 3,
    1, 1, 2, 2, 3, 3, 4, 4, 5,
    1, 1, 2, 2, 3, 3, 4, 4, 5,
    1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6,
    1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4,
    2, 2,
    2, 2,
    2, 2, 4, 4,
    2, 2, 3, 3, 5, 5,
    5, 5,
    4,
    5,
    2, 3, 4, 6);

  LINE_WIDTH: Byte = 40;

  TEXT_LINE_ADDR: array[0..9] of Word = (
    TEXT_ADDR + 0 * LINE_WIDTH, TEXT_ADDR + 1 * LINE_WIDTH,
    TEXT_ADDR + 2 * LINE_WIDTH, TEXT_ADDR + 3 * LINE_WIDTH,
    TEXT_ADDR + 4 * LINE_WIDTH, TEXT_ADDR + 5 * LINE_WIDTH,
    TEXT_ADDR + 6 * LINE_WIDTH, TEXT_ADDR + 7 * LINE_WIDTH,
    TEXT_ADDR + 8 * LINE_WIDTH, TEXT_ADDR + 9 * LINE_WIDTH);

var
  sss: TString;

function GetActionCaption(action: TAction): TCaption;
begin
  if action = actController then
  begin
    case menuControllerKind of
      ckAtariMouse: Result := capAtariMouse;
      ckAmigaMouse: Result := capAmigaMouse;
      ckTrakball: Result := capTrakball;
      else Result := capJoystick;
    end
  end
  else if action = actControllerPort then
  begin
    if menuControllerPortTwo then Result := capTwo
    else Result := capOne;
  end
  else if action = actFastProcessing then
  begin
    if menuFastProcessing then Result := capOn
    else Result := capOff;
  end
  else begin    
    Result := TCaption(ACTION_CAPTIONS[action]);
  end;
end;

procedure HighlightMenuItem;
var
  i, x: Byte;
  width: Byte;
  addr: Word;
  action: TAction;
begin
  action := TAction(menuItems[menuItemIndex]);

  width := CAPTIONS_LENGTHS[Ord(GetActionCaption(TAction(action)))] + 2;
  x := MENU_ITEM_X[action] - 1;
  addr := TEXT_LINE_ADDR[MENU_ITEM_Y[action]] + x;
  for i := 1 to width do
  begin
    Poke(addr, Peek(addr) xor $80);
    Inc(addr);
  end;
end;

procedure UpdateMenuItemIndex;
begin
  if newMenuItemIndex <> menuItemIndex then
  begin
    HighlightMenuItem;
    menuItemIndex := newMenuItemIndex;
    HighlightMenuItem;
  end;
end;

procedure UpdateIntensity;
var
  len: Byte;
const
  ADDR_VALUE_STR: Word = TEXT_ADDR + LINE_WIDTH * 2 + 10;
  ADDR_VALUE_VAL: Word = TEXT_ADDR + LINE_WIDTH * 2 + 20;
begin
  Move(Pointer(CAPTIONS[capValue]), Pointer(ADDR_VALUE_STR), CAPTIONS_LENGTHS[capValue]);
  sss := IntToAnticString(intensityVal);
  len := Byte(sss[0]);
  Move(@sss[1], Pointer(ADDR_VALUE_VAL), len);
  Poke(ADDR_VALUE_VAL + len, Byte(' '~));
end;

procedure UpdateBrightnessContrast;
var
  addr: Word;
  len: Byte;
const
  ADDR_BR_STR: Word = TEXT_ADDR + LINE_WIDTH * 2 + 9;
  ADDR_BR_VAL: Word = TEXT_ADDR + LINE_WIDTH * 2 + 20;
  ADDR_CON_STR: Word = TEXT_ADDR + LINE_WIDTH * 3 + 11;
  ADDR_CON_VAL: Word = TEXT_ADDR + LINE_WIDTH * 3 + 20;
begin
  addr := CAPTIONS[capBrightnessContrast];
  Move(Pointer(addr), Pointer(ADDR_BR_STR), 10);
  Move(Pointer(addr + 11), Pointer(ADDR_CON_STR), 8);

  sss := IntToAnticString(brightness - 16);
  len := Byte(sss[0]);
  Move(@sss[1], Pointer(ADDR_BR_VAL), len);
  Poke(ADDR_BR_VAL + len, Byte(' '~));

  sss := IntToAnticString(contrast - 16);
  len := Byte(sss[0]);
  Move(@sss[1], Pointer(ADDR_CON_VAL), len);
  Poke(ADDR_CON_VAL + len, Byte(' '~));
end;

procedure UpdateLoadSave;
var
  addr: Word;
  ptr: PByte;
  i, len: Byte;
const
  ADDR_CAPTION: Word = TEXT_ADDR + LINE_WIDTH * 2 + 1;
  ADDR_FILEPATH: Word = TEXT_ADDR + LINE_WIDTH * 3 + 1;
  ADDR_ERROR: Word = TEXT_ADDR + LINE_WIDTH * 4 + 1;
begin
  addr := CAPTIONS[capEnterFilePath];
  Move(Pointer(addr), Pointer(ADDR_CAPTION), 16);
  
  ptr := @filePath;
  len := ptr^;
  addr := ADDR_FILEPATH;
  Inc(ptr);
  i := 0;
  while i < len do
  begin
    case (ptr^ and $7f) of
      0..31: Poke(addr, ptr^ + 64);
      32..95: Poke(addr, ptr^ - 32);
      else Poke(addr, ptr^);
    end;
    Inc(i);
    Inc(ptr);
    Inc(addr);
  end;
  Poke(addr, Byte(' '~*));
  Inc(addr);
  Poke(addr, Byte(' '~));
end;

procedure EditFilePath;
var
  len: Byte;
const
  KEYCLK = $F983;
  KEY_DEL: Byte = 126;

begin
  len := Byte(filePath[0]);
  if key = Char(KEY_DEL) then
  begin
    if len = 0 then Exit;

    Dec(len);
  end
  else begin
    if len > 30 then Exit;

    if (key >= 'a') and (key <= 'z') then
    begin
      key := Char(Byte(key) - 32);
    end;

    if ((key < '0') or (key > '9')) and ((key < 'A') or (key > 'Z')) and
      (key <> '.') and (key <> '_') and (key <> ':') then Exit;

    Inc(len);
    filePath[len] := key;
  end;

  filePath[0] := Char(len);
  UpdateLoadSave;

  asm { jsr KEYCLK };
end;

procedure UpdateIOError;
var
  addr: Word;
  ptr: PByte;
  i, len: Byte;
const
  ADDR_ERROR_STR: Word = TEXT_ADDR + LINE_WIDTH * 2 + 7;
  ADDR_ERROR_VAL: Word = TEXT_ADDR + LINE_WIDTH * 2 + 27;
begin
  addr := CAPTIONS[capIOErrorOccurred];
  Move(Pointer(addr), Pointer(ADDR_ERROR_STR), 20);

  sss := IntToAnticString(lastIOResult);
  len := Byte(sss[0]);
  Move(@sss[1], Pointer(ADDR_ERROR_VAL), len);
end;

procedure UpdateHistogram;
const
  BOTTOM_LINE_ADDR: Word = TEXT_ADDR + LINE_WIDTH * 9 + 1;
  LABELS: array[0..MAX_LUMINANCE] of Char = (
    '0'~, '1'~, '2'~, '3'~, '4'~, '5'~, '6'~, '7'~,
    '8'~, '9'~, 'A'~, 'B'~, 'C'~, 'D'~, 'E'~, 'F'~);
  PARTS: array[0..3] of Char = (
    #0, #78, #85, #205);
  TEMP_ADDR: Word = TEXT_ADDR + LINE_WIDTH * 2 + 1;
var
  i: Byte;
  val: Byte;
  addr, addr2: Word;
begin
  addr := BOTTOM_LINE_ADDR;
  i := 0;
  while i <= MAX_LUMINANCE do
  begin
    Poke(addr, Byte(LABELS[i]));

    addr2 := addr;
    Dec(addr2, LINE_WIDTH);
    val := HistogramNorm[i] shr 2;
    while val > 0 do
    begin
      Poke(addr2, Byte(' '~*));
      Dec(addr2, LINE_WIDTH);
      Dec(val);
    end;

    val := HistogramNorm[i] and 3;
    if val > 0 then
    begin
      Poke(addr2, Byte(PARTS[val]));
    end;

    Inc(addr, 2);
    Inc(i);
  end;

  // debug
  {addr := TEMP_ADDR;
  for i := 0 to 15 do
  begin
    sss := IntToAnticString(Word(HistogramLo[i]) + Word(HistogramHi[i]) * 256);
    val := Byte(sss[0]);
    Move(@sss[1], Pointer(addr), val);
    Inc(addr, val + 1);
  end;}
end;

procedure UpdateSettings;
var
  len: Byte;
const
  ADDR_CONTROLLER_STR: Word = TEXT_ADDR + LINE_WIDTH * 2 + 8;
  ADDR_CONTROLLER_PORT_STR: Word = TEXT_ADDR + LINE_WIDTH * 3 + 3;
  ADDR_FAST_PROCESSING_STR: Word = TEXT_ADDR + LINE_WIDTH * 4 + 3;
begin
  Move(Pointer(CAPTIONS[capController]), Pointer(ADDR_CONTROLLER_STR), CAPTIONS_LENGTHS[capController]);
  Move(Pointer(CAPTIONS[capControllerPort]), Pointer(ADDR_CONTROLLER_PORT_STR), CAPTIONS_LENGTHS[capControllerPort]);
  Move(Pointer(CAPTIONS[capFastProcessing]), Pointer(ADDR_FAST_PROCESSING_STR), CAPTIONS_LENGTHS[capFastProcessing]);
end;

procedure UpdateMenu;
var
  menuHeight: Byte;
  i, x: Byte;
  action: TAction;
  addr: Word;
  caption: TCaption;
  len, pos: Byte;
const
  SIZE = LINE_WIDTH * 10;
begin
  menuHeight := MENU_HEIGHT[menuKind];
  FillChar(Pointer(TEXT_ADDR), SIZE, ' '~);

  if menuKind = mkIntensity then
  begin
    UpdateIntensity;
  end
  else if menuKind = mkBrightnessContrast then
  begin
    UpdateBrightnessContrast;
  end
  else if menuKind = mkLoadSave then
  begin
    UpdateLoadSave;
  end
  else if menuKind = mkLoadSaveError then
  begin
    UpdateIOError;
  end
  else if menuKind = mkHistogram then
  begin
    UpdateHistogram;
  end
  else if menuKind = mkSettings then
  begin
    UpdateSettings;
  end;

  if menuHeight > 1 then
  begin
    FillChar(Pointer(TEXT_ADDR), LINE_WIDTH, HORIZONTAL_LINE);

    caption := TCaption(MENU_CAPTIONS[menuKind]);
    if (menuKind = mkLoadSave) and (loadSaveAction = lsaSave) then
    begin
      caption := capSave;
    end
    else if menuKind = mkIntensity then
    begin
      caption := intensityCaption;
    end;
    len := CAPTIONS_LENGTHS[caption];
    x := LINE_WIDTH;
    Dec(x, len);
    x := x shr 1;
    Move(Pointer(CAPTIONS[caption]), Pointer(TEXT_ADDR + x), len);
  end;

  i := 0;
  while i < menuItemCount do
  begin
    action := TAction(menuItems[i]);

    addr := TEXT_LINE_ADDR[MENU_ITEM_Y[action]];
    Inc(addr, MENU_ITEM_X[action]);

    caption := GetActionCaption(TAction(action));
    Move(Pointer(CAPTIONS[caption]), Pointer(addr), CAPTIONS_LENGTHS[caption]);

    pos := ACTION_SHORTCUT_INDEX[action];
    if pos < 255 then
    begin
      Inc(addr, pos);
      Poke(addr, Peek(addr) xor %10000000);
    end;
    
    Inc(i)
  end;
  
  HighlightMenuItem;
end;

procedure GetMenuPos;
var
  x, y, i: Byte;
  pos: Byte;
  action: TAction;
begin
  x := cursorX;
  Dec(x, MIN_CURSOR_X);
  x := x shr 2;
  y := cursorY;
  Dec(y, MIN_CURSOR_Y);
  y := y shr 3;

  i := 0;
  while i < menuItemCount do
  begin
    action := TAction(menuItems[i]);
    pos := MENU_ITEM_X[action] - 1;
    if y = MENU_ITEM_Y[action] then
      if x >= pos then
      begin
        Inc(pos);
        Inc(pos, CAPTIONS_LENGTHS[ACTION_CAPTIONS[action]]);
        if x <= pos then
        begin
          newMenuItemIndex := i;
          UpdateMenuItemIndex;
          menuPosPosSuccess := True;
          Exit;
        end;
      end;

    Inc(i);
  end;

  menuPosPosSuccess := False;
end;

procedure IntensityDec;
begin
  if intensityVal > 0 then
  begin
    Dec(intensityVal);
    UpdateIntensity;
  end;
end;

procedure IntensityInc;
begin
  if intensityVal < MAX_LUT then
  begin
    Inc(intensityVal);
    UpdateIntensity;
  end;
end;

procedure BrightnessDec;
begin
  if brightness > 1 then
  begin
    Dec(brightness);
    UpdateBrightnessContrast;
  end;
end;

procedure BrightnessInc;
begin
  if brightness < 31 then
  begin
    Inc(brightness);
    UpdateBrightnessContrast;
  end;
end;

procedure ContrastDec;
begin
  if contrast > 1 then
  begin
    Dec(contrast);
    UpdateBrightnessContrast;
  end;
end;

procedure ContrastInc;
begin
  if contrast < 31 then
  begin
    Inc(contrast);
    UpdateBrightnessContrast;
  end;
end;

procedure CopyMenuItems;
var
  src, dst: Byte;
begin
  menuItemCount := MENU_ITEM_COUNT[menuKind];
  src := MENU_ITEMS_FIRST[menuKind];
  dst := 0;
  while dst < menuItemCount do
  begin
    menuItems[dst] := src;
    Inc(src);
    Inc(dst);
  end;
end;

end.