function_add_noise

.local

    phr

    mva DST_PTR_ADDR ZP_FIRST_ADDR
    mva DST_PTR_ADDR+1 ZP_FIRST_ADDR+1

    ;for y := 1 to IMAGE_HEIGHT do
    mva ROI_HEIGHT_ADDR y

loop

    ;for x := 1 to IMAGE_STEP do
    ldy ROI_WIDTH_BYTES_ADDR
    dey
    sty x

internal_loop

    ;value := Peek(Word(dstPtrLine));
    lda (ZP_FIRST_ADDR),y
    tax

    ;right := value and $0f;
    and #$0F
    jsr add_noise_internal
    sta right

    ;left := LUT[value shr 4];
    txa
    lsr
    lsr
    lsr
    lsr
    jsr add_noise_internal

    ;Poke(Word(DST_PTR_ADDR), (left shl 4) or right);
    asl
    asl
    asl
    asl
    ora right
    ldy x
    sta (ZP_FIRST_ADDR),y

    dey
    sty x
    bpl internal_loop

    ;Inc(ZP_FIRST_ADDR, IMAGE_STEP);
    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    dec y
    bne loop

    plr
    rts

add_noise_internal

    tay
    lda random

    cmp #$20
    bcs increase_value

    dey
    bpl end_of_add_noise
    ldy #0
    jmp end_of_add_noise

increase_value

    cmp #$E0
    bcc end_of_add_noise

    iny
    cpy #$10
    bne end_of_add_noise
    ldy #$0F

end_of_add_noise

    tya
    rts

x equ $e8
right equ $e9
y equ $ea
random equ $d20a

.endl