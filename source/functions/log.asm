function_log

.local

    phr

    ldy #MAX_LUMINANCE

loop

    mva log_array,y LUT_ARRAY_ADDR,y
    dey
    bpl loop

    jsr function_lut

    plr
    rts

log_array

    dta 0, 0, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3

.endl