function_divide_by_value

.local

    phr

    ldx #MAX_LUMINANCE

loop

    stx al
    mva INTENSITY_VALUE_ADDR cl
    jsr idivAL_CL
    lda :eax
    sta LUT_ARRAY_ADDR,x

    dex
    bpl loop

    jsr function_lut

    plr
    rts

.endl