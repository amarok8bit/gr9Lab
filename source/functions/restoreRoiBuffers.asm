function_restore_roi_buffers

.local

    phr

    ldx #0
    lda ROI_LEFT_MASK_ADDR
    cmp #$F0
    beq check_right_roi

    mva DST_PTR_ADDR ZP_FIRST_ADDR
    mva DST_PTR_ADDR+1 ZP_FIRST_ADDR+1

    mva #.lo(LEFT_ROI_BUFFER_ADDR) ZP_SECOND_ADDR
    mva #.hi(LEFT_ROI_BUFFER_ADDR) ZP_SECOND_ADDR+1

    ldy #0

left_loop

    lda (ZP_FIRST_ADDR,x)
    and #$0F
    ora (ZP_SECOND_ADDR),y
    sta (ZP_FIRST_ADDR,x)

    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    iny
    cpy ROI_HEIGHT_ADDR
    bne left_loop

check_right_roi

    lda ROI_RIGHT_MASK_ADDR
    cmp #$0F
    beq end_of_roi_check

    mva DST_PTR_ADDR ZP_FIRST_ADDR
    mva DST_PTR_ADDR+1 ZP_FIRST_ADDR+1
    lda ZP_FIRST_ADDR
    add ROI_WIDTH_BYTES_MINUS_ONE_ADDR
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    mva #.lo(RIGHT_ROI_BUFFER_ADDR) ZP_SECOND_ADDR
    mva #.hi(RIGHT_ROI_BUFFER_ADDR) ZP_SECOND_ADDR+1

    ldy #0

right_loop

    lda (ZP_FIRST_ADDR,x)
    and #$F0
    ora (ZP_SECOND_ADDR),y
    sta (ZP_FIRST_ADDR,x)

    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    iny
    cpy ROI_HEIGHT_ADDR
    bne right_loop

end_of_roi_check

    plr
    rts

.endl