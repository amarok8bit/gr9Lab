function_sqrt

.local

    phr

    ldy #MAX_LUMINANCE

loop

    mva sqrt_array,y LUT_ARRAY_ADDR,y
    dey
    bpl loop

    jsr function_lut

    plr
    rts

sqrt_array

    dta 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 4

.endl