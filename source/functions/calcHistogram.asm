function_calc_histogram

.local

    phr
    mva DST_PTR_ADDR ZP_FIRST_ADDR
    mva DST_PTR_ADDR+1 ZP_FIRST_ADDR+1

    ldy #MAX_LUMINANCE
    lda #0

clear_loop

    sta HISTOGRAM_LO_ARRAY_ADDR,y
    sta HISTOGRAM_HI_ARRAY_ADDR,y
    dey
    bpl clear_loop

    ;for y := 1 to IMAGE_HEIGHT do
    mva ROI_HEIGHT_ADDR y

loop

    ;for x := 1 to IMAGE_STEP do
    ldy ROI_WIDTH_BYTES_ADDR
    dey
    sty x

internal_loop

    ;value := Peek(Word(dstPtrLine));
    lda (ZP_FIRST_ADDR),y
    tax

    ;Inc(Histogram[value and $0f]);
    and #$0F 
    tay

    lda HISTOGRAM_LO_ARRAY_ADDR,y
    add #1
    sta HISTOGRAM_LO_ARRAY_ADDR,y
    bcc right_less_than_255
    lda HISTOGRAM_HI_ARRAY_ADDR,y
    add #1
    sta HISTOGRAM_HI_ARRAY_ADDR,y

right_less_than_255

    ;Inc(Histogram[value shr 4]);
    txa
    lsr
    lsr
    lsr
    lsr
    tay

    lda HISTOGRAM_LO_ARRAY_ADDR,y
    add #1
    sta HISTOGRAM_LO_ARRAY_ADDR,y
    bcc left_less_than_255
    lda HISTOGRAM_HI_ARRAY_ADDR,y
    add #1
    sta HISTOGRAM_HI_ARRAY_ADDR,y

left_less_than_255

    ldy x
    dey
    sty x
    bpl internal_loop

check_left_roi

    lda ROI_LEFT_MASK_ADDR
    cmp #$F0
    beq check_right_roi

    ldy #0
    lda (ZP_FIRST_ADDR),y
    lsr
    lsr
    lsr
    lsr
    tay

    lda HISTOGRAM_LO_ARRAY_ADDR,y
    sub #1
    sta HISTOGRAM_LO_ARRAY_ADDR,y
    bcs check_right_roi
    lda HISTOGRAM_HI_ARRAY_ADDR,y
    sub #1
    sta HISTOGRAM_HI_ARRAY_ADDR,y

check_right_roi

    lda ROI_RIGHT_MASK_ADDR
    cmp #$0F
    beq end_of_roi_check

    ldy ROI_WIDTH_BYTES_ADDR
    dey
    lda (ZP_FIRST_ADDR),y
    and #$0F
    tay

    lda HISTOGRAM_LO_ARRAY_ADDR,y
    sub #1
    sta HISTOGRAM_LO_ARRAY_ADDR,y
    bcs end_of_roi_check
    lda HISTOGRAM_HI_ARRAY_ADDR,y
    sub #1
    sta HISTOGRAM_HI_ARRAY_ADDR,y

end_of_roi_check

    ;Inc(ZP_FIRST_ADDR, IMAGE_STEP);
    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    dec y
    beq end_loop
    jmp loop

end_loop

    plr
    rts

x equ $e8
y equ $ea

.endl