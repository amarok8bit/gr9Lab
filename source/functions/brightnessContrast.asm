function_brightness_contrast

.local

    phr

    ldy #0
    sty tmp1
    sty tmp1+1

loop

    mva tmp1 tmp2
    mva tmp1+1 tmp2+1

    ;tmp2 := tmp1 shr 4
    lsr tmp2+1
    ror tmp2
    lsr tmp2+1
    ror tmp2
    lsr tmp2+1
    ror tmp2
    lsr tmp2+1
    ror tmp2

    ;Inc(tmp2, brightness)
    lda tmp2
    add BRIGHTNESS_VALUE_ADDR

    ;Dec(tmp2, 16)
    sub #16

    bpl value_not_negative
    lda #0
    jmp value_ok

value_not_negative

    cmp #MAX_LUMINANCE
    bmi value_ok
    lda #MAX_LUMINANCE

value_ok

    sta LUT_ARRAY_ADDR,y

    ;Inc(tmp1, contrast)
    lda tmp1
    add CONTRAST_VALUE_ADDR
    sta tmp1
    scc
    inc tmp1+1

    iny
    cpy #MAX_LUMINANCE+1
    bne loop

    jsr function_lut

    plr
    rts

tmp1 equ $e8
tmp2 equ $ea

.endl