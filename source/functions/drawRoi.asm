function_draw_roi

.local

    phr

    mva DST_PTR_ADDR ZP_FIRST_ADDR
    mva DST_PTR_ADDR+1 ZP_FIRST_ADDR+1

    ;if ROI_WIDTH_ADDR = 1 then
    lda ROI_WIDTH_ADDR
    cmp #1
    bne horizontal_start

    ;for y := 0 to ROI_HEIGHT_ADDR - 1 do
    ldx ROI_HEIGHT_ADDR
    ldy #0

vertical_loop_1

    lda (ZP_FIRST_ADDR),y
    eor ROI_LEFT_MASK_ADDR
    sta (ZP_FIRST_ADDR),y

    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    dex
    beq end_loop_1

    jmp vertical_loop_1

end_loop_1

    jmp finish

horizontal_start

    ;if ROI_HEIGHT_ADDR = 1 then
    lda ROI_HEIGHT_ADDR
    cmp #1
    bne height_greater_than_1

    ;for x := 1 to ROI_WIDTH_BYTES_ADDR do
    ldy ROI_WIDTH_BYTES_ADDR
    dey

    ;if ROI_RIGHT_MASK_ADDR = $F0 then
    ;begin
    ;Poke(Word(ZP_FIRST_ADDR), Peek(Word(ZP_FIRST_ADDR)) xor $f0)
    ;end
    lda ROI_RIGHT_MASK_ADDR
    cmp #$F0
    bne horizontal_loop_1

    lda (ZP_FIRST_ADDR),y
    eor #$F0
    sta (ZP_FIRST_ADDR),y

    dey

horizontal_loop_1

    cpy #0
    beq horizontal_loop_end_1

    ;Poke(Word(ZP_FIRST_ADDR), Peek(Word(ZP_FIRST_ADDR)) xor $ff);
    lda (ZP_FIRST_ADDR),y
    eor #$FF
    sta (ZP_FIRST_ADDR),y

    dey
    ;bpl horizontal_loop_1
    jmp horizontal_loop_1

horizontal_loop_end_1

    ;if ROI_LEFT_MASK_ADDR = $0F then
    ;begin
    ;Poke(Word(ZP_FIRST_ADDR), Peek(Word(ZP_FIRST_ADDR)) xor $0f)
    ;end
    ;else begin
    ;Poke(Word(ZP_FIRST_ADDR), Peek(Word(ZP_FIRST_ADDR)) xor $ff)
    ;end;

    lda ROI_LEFT_MASK_ADDR
    cmp #$0F
    beq left_mask_0f_1

    lda (ZP_FIRST_ADDR),y
    eor #$FF
    sta (ZP_FIRST_ADDR),y

    jmp horizontal_end_1

left_mask_0f_1

    lda (ZP_FIRST_ADDR),y
    eor #$0F
    sta (ZP_FIRST_ADDR),y

horizontal_end_1

    jmp finish

height_greater_than_1

    ;ZP_SECOND_ADDR := ZP_FIRST_ADDR + IMAGE_STEP * (ROI_HEIGHT_ADDR - 1);
    ldy ROI_HEIGHT_ADDR
    dey
    sty :ecx
    mva #0 :ecx+1
    mva #IMAGE_STEP :eax
    mva #0 :eax+1
    imulCX

    lda ZP_FIRST_ADDR
    add :eax
    sta ZP_SECOND_ADDR
    lda ZP_FIRST_ADDR+1
    adc :eax+1
    sta ZP_SECOND_ADDR+1

    ;for x := 1 to ROI_WIDTH_BYTES_ADDR do
    ldy ROI_WIDTH_BYTES_ADDR
    dey

    ;if ROI_RIGHT_MASK_ADDR = $F0 then
    ;begin
    ;Poke(Word(ZP_FIRST_ADDR), Peek(Word(ZP_FIRST_ADDR)) xor $f0)
    ;Poke(Word(ZP_SECOND_ADDR), Peek(Word(ZP_SECOND_ADDR)) xor $f0);
    ;end
    lda ROI_RIGHT_MASK_ADDR
    cmp #$F0
    bne horizontal_loop

    lda (ZP_FIRST_ADDR),y
    eor #$F0
    sta (ZP_FIRST_ADDR),y

    lda (ZP_SECOND_ADDR),y
    eor #$F0
    sta (ZP_SECOND_ADDR),y

    dey

horizontal_loop

    cpy #0
    beq horizontal_loop_end

    ;Poke(Word(ZP_FIRST_ADDR), Peek(Word(ZP_FIRST_ADDR)) xor $ff);
    lda (ZP_FIRST_ADDR),y
    eor #$FF
    sta (ZP_FIRST_ADDR),y

    ;Poke(Word(ZP_SECOND_ADDR), Peek(Word(ZP_SECOND_ADDR)) xor $ff);
    lda (ZP_SECOND_ADDR),y
    eor #$FF
    sta (ZP_SECOND_ADDR),y

    dey
    ;bpl horizontal_loop
    jmp horizontal_loop

horizontal_loop_end

    ;if ROI_LEFT_MASK_ADDR = $0F then
    ;begin
    ;Poke(Word(ZP_FIRST_ADDR), Peek(Word(ZP_FIRST_ADDR)) xor $0f)
    ;Poke(Word(ZP_SECOND_ADDR), Peek(Word(ZP_SECOND_ADDR)) xor $0f);
    ;end
    ;else begin
    ;Poke(Word(ZP_FIRST_ADDR), Peek(Word(ZP_FIRST_ADDR)) xor $ff)
    ;Poke(Word(ZP_SECOND_ADDR), Peek(Word(ZP_SECOND_ADDR)) xor $ff);
    ;end;

    lda ROI_LEFT_MASK_ADDR
    cmp #$0F
    beq left_mask_0f

    lda (ZP_FIRST_ADDR),y
    eor #$FF
    sta (ZP_FIRST_ADDR),y

    lda (ZP_SECOND_ADDR),y
    eor #$FF
    sta (ZP_SECOND_ADDR),y

    jmp horizontal_end

left_mask_0f

    lda (ZP_FIRST_ADDR),y
    eor #$0F
    sta (ZP_FIRST_ADDR),y

    lda (ZP_SECOND_ADDR),y
    eor #$0F
    sta (ZP_SECOND_ADDR),y

horizontal_end

    ;ZP_SECOND_ADDR := ZP_FIRST_ADDR + ROI_WIDTH_BYTES_ADDR - 1;
    mva ZP_FIRST_ADDR+1 ZP_SECOND_ADDR+1
    ldy ROI_WIDTH_BYTES_ADDR
    dey
    tya
    add ZP_FIRST_ADDR
    sta ZP_SECOND_ADDR
    scc
    inc ZP_SECOND_ADDR+1

    ;for y := 2 to ROI_HEIGHT_ADDR - 1 do
    ldx ROI_HEIGHT_ADDR
    dex
    ldy #0

vertical_loop

    dex
    beq end_loop

    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    lda ZP_SECOND_ADDR
    add #IMAGE_STEP
    sta ZP_SECOND_ADDR
    scc
    inc ZP_SECOND_ADDR+1

    lda (ZP_FIRST_ADDR),y
    eor ROI_LEFT_MASK_ADDR
    sta (ZP_FIRST_ADDR),y
    
    lda (ZP_SECOND_ADDR),y
    eor ROI_RIGHT_MASK_ADDR
    sta (ZP_SECOND_ADDR),y

    jmp vertical_loop

end_loop

finish

    plr
    rts

y equ $e8

.endl