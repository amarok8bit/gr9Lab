function_image_blender

.local

    phr

    lda IMAGE_BLENDER_OPERATION_ADDR
    asl
    tay
    mva right_operators,y right_operator+1
    mva right_operators+1,y right_operator+2
    mva left_operators,y left_operator+1
    mva left_operators+1,y left_operator+2    

    mva SRC_PTR_ADDR ZP_FIRST_ADDR
    mva SRC_PTR_ADDR+1 ZP_FIRST_ADDR+1

    mva SRC2_PTR_ADDR ZP_SECOND_ADDR
    mva SRC2_PTR_ADDR+1 ZP_SECOND_ADDR+1

    mva DST_PTR_ADDR ZP_THIRD_ADDR
    mva DST_PTR_ADDR+1 ZP_THIRD_ADDR+1

    ;for y := 1 to IMAGE_HEIGHT do
    mva ROI_HEIGHT_ADDR y

loop

    ;for x := 1 to IMAGE_STEP do
    ldy ROI_WIDTH_BYTES_ADDR
    dey

internal_loop

    ;value1 := Peek(Word(SRC_PTR_ADDRLine));
    lda (ZP_FIRST_ADDR),y
    tax

    ;right := value1 and $0f;
    and #$0F 
    sta right

    ;left := value1 shr 4;
    txa
    lsr
    lsr
    lsr
    lsr
    sta left

    ;value2 := Peek(Word(SRC2_PTR_ADDRLine));
    lda (ZP_SECOND_ADDR),y
    tax

    ;right := (right + value1 and $0f) shr 1;
    and #$0F

right_operator

    jsr right_average_operator
    sta right

    ;left := (left + value1 shr 4) shr 1;
    txa
    lsr
    lsr
    lsr
    lsr

left_operator

    jsr left_average_operator

    ;Poke(Word(DST_PTR_ADDRLine), (left shl 4) or right);
    asl
    asl
    asl
    asl
    ora right
    sta (ZP_THIRD_ADDR),y

    dey
    bpl internal_loop

    ;Inc(ZP_FIRST_ADDR, IMAGE_STEP);
    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    ;Inc(ZP_SECOND_ADDR, IMAGE_STEP);
    lda ZP_SECOND_ADDR
    add #IMAGE_STEP
    sta ZP_SECOND_ADDR
    scc
    inc ZP_SECOND_ADDR+1

    ;Inc(ZP_THIRD_ADDR, IMAGE_STEP);
    lda ZP_THIRD_ADDR
    add #IMAGE_STEP
    sta ZP_THIRD_ADDR
    scc
    inc ZP_THIRD_ADDR+1

    dec y
    bne loop

    plr
    rts

right_operators

    dta a(right_add_operator)
    dta a(right_subtract_operator)
    dta a(right_difference_operator)
    dta a(right_multiply_operator)
    dta a(right_divide_operator)
    dta a(right_average_operator)
    dta a(right_or_operator)
    dta a(right_and_operator)
    dta a(right_xor_operator)
    dta a(right_minimum_operator)
    dta a(right_maximum_operator)

left_operators

    dta a(left_add_operator)
    dta a(left_subtract_operator)
    dta a(left_difference_operator)
    dta a(left_multiply_operator)
    dta a(left_divide_operator)
    dta a(left_average_operator)
    dta a(left_or_operator)
    dta a(left_and_operator)
    dta a(left_xor_operator)
    dta a(left_minimum_operator)
    dta a(left_maximum_operator)

right_add_operator

    add right
    cmp #MAX_LUMINANCE
    bmi right_sum_is_ok
    lda #MAX_LUMINANCE

right_sum_is_ok

    rts

left_add_operator

    add left
    cmp #MAX_LUMINANCE
    bmi left_sum_is_ok
    lda #MAX_LUMINANCE

left_sum_is_ok

    rts

right_subtract_operator

    sub right
    bpl right_subtract_is_ok
    lda #0

right_subtract_is_ok

    rts

left_subtract_operator

    sub left
    bpl left_subtract_is_ok
    lda #0

left_subtract_is_ok

    rts

right_difference_operator

    sub right
    bpl right_difference_is_ok
    eor #$FF

right_difference_is_ok

    rts

left_difference_operator

    sub left
    bpl left_difference_is_ok
    eor #$FF

left_difference_is_ok

    rts

right_multiply_operator

    sta :ecx
    mva right :eax
    
    sty x
    imulCL
    
    lda :eax
    cmp #MAX_LUMINANCE+1
    bcc right_multiply_is_ok
    lda #MAX_LUMINANCE

right_multiply_is_ok

    rts

left_multiply_operator

    sta :ecx
    mva left :eax

    imulCL
    ldy x
    
    lda :eax
    cmp #MAX_LUMINANCE+1
    bcc left_multiply_is_ok
    lda #MAX_LUMINANCE

left_multiply_is_ok

    rts

right_divide_operator

    sta cl
    mva right al

    sty x
    jsr idivAL_CL

    lda :eax
    rts

left_divide_operator

    sta cl
    mva left al

    jsr idivAL_CL
    ldy x

    lda :eax
    rts

right_average_operator

    add right
    lsr
    rts

left_average_operator

    add left
    lsr
    rts

right_or_operator

    ora right
    rts

left_or_operator

    ora left
    rts

right_and_operator

    and right
    rts

left_and_operator

    and left
    rts

right_xor_operator

    eor right
    rts

left_xor_operator

    eor left
    rts

right_minimum_operator

    cmp right
    bmi right1_is_greater
    lda right

right1_is_greater

    rts

left_minimum_operator

    cmp left
    bmi left1_is_greater
    lda left

left1_is_greater

    rts
    
right_maximum_operator

    cmp right
    bpl right2_is_greater
    lda right

right2_is_greater

    rts

left_maximum_operator

    cmp left
    bpl left2_is_greater
    lda left

left2_is_greater

    rts
    
right equ $e8
left equ $e9
y equ $ea
x equ $eb

.endl