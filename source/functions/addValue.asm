function_add_value

.local

    phr

    ldy #MAX_LUMINANCE

loop

    tya
    add INTENSITY_VALUE_ADDR
    cmp #MAX_LUMINANCE
    bmi value_ok

    lda #MAX_LUMINANCE

value_ok

    sta LUT_ARRAY_ADDR,y

    dey
    bpl loop

    jsr function_lut

    plr
    rts

.endl