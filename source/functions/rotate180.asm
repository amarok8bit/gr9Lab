function_rotate_180

.local

    phr

    jsr function_flip_vertical
    jsr function_flip_horizontal

    plr
    rts
    
.endl