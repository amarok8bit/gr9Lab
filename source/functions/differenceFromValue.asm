function_difference_from_value

.local

    phr

    ldy #MAX_LUMINANCE

loop

    tya
    sub INTENSITY_VALUE_ADDR
    bpl value_ok

    eor #$FF

value_ok

    sta LUT_ARRAY_ADDR,y

    dey
    bpl loop

    jsr function_lut

    plr
    rts

.endl