function_set

.local

    phr

    mva DST_PTR_ADDR ZP_FIRST_ADDR
    mva DST_PTR_ADDR+1 ZP_FIRST_ADDR+1

    ;for y := 1 to IMAGE_HEIGHT do
    mva ROI_HEIGHT_ADDR y

    lda INTENSITY_VALUE_ADDR
    asl
    asl
    asl
    asl
    ora INTENSITY_VALUE_ADDR
    tax

loop

    ;for x := 1 to IMAGE_STEP do
    ldy ROI_WIDTH_BYTES_ADDR
    dey
    txa

internal_loop

    sta (ZP_FIRST_ADDR),y

    dey
    bpl internal_loop

    ;Inc(ZP_FIRST_ADDR, IMAGE_STEP);
    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    dec y
    bne loop

    plr
    rts

y equ $e8

.endl