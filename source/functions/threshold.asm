function_threshold

.local

    phr

    ldy #MAX_LUMINANCE

loop

    tya
    cmp INTENSITY_VALUE_ADDR
    bpl is_greater
    lda #0
    jmp value_ok

is_greater 

    lda #MAX_LUMINANCE

value_ok

    sta LUT_ARRAY_ADDR,y

    dey
    bpl loop

    jsr function_lut

    plr
    rts

.endl