function_box_filter

.local

    phr

    ;FIRST_DST_LINE_ADDR := LINE_1_ADDR;
    ;SECOND_DST_LINE_ADDR := LINE_2_ADDR;
    ;THIRD_DST_LINE_ADDR := LINE_3_ADDR;
    mva #.lo(LINE_1_ADDR) FIRST_DST_LINE_ADDR
    mva #.hi(LINE_1_ADDR) FIRST_DST_LINE_ADDR+1
    mva #.lo(LINE_2_ADDR) SECOND_DST_LINE_ADDR
    mva #.hi(LINE_2_ADDR) SECOND_DST_LINE_ADDR+1
    mva #.lo(LINE_3_ADDR) THIRD_DST_LINE_ADDR
    mva #.hi(LINE_3_ADDR) THIRD_DST_LINE_ADDR+1

    ;ZP_FOURTH_ADDR := DST_PTR_ADDR; 
    mva DST_PTR_ADDR ZP_FOURTH_ADDR
    mva DST_PTR_ADDR+1 ZP_FOURTH_ADDR+1

    ;ZP_FIRST_ADDR := Word(DST_PTR_ADDR);
    ;TMP_FIRST_ADDR := ZP_FIRST_ADDR;
    ;ZP_SECOND_ADDR := SECOND_DST_LINE_ADDR;
    ;LineSum111Fast
    mva DST_PTR_ADDR ZP_FIRST_ADDR
    mva DST_PTR_ADDR+1 ZP_FIRST_ADDR+1
    mva ZP_FIRST_ADDR TMP_FIRST_ADDR
    mva ZP_FIRST_ADDR+1 TMP_FIRST_ADDR+1
    mva SECOND_DST_LINE_ADDR ZP_SECOND_ADDR
    mva SECOND_DST_LINE_ADDR+1 ZP_SECOND_ADDR+1

    jsr function_line_sum_111

    ;ZP_FIRST_ADDR := SECOND_DST_LINE_ADDR;
    ;ZP_SECOND_ADDR := FIRST_DST_LINE_ADDR;
    ;CopyLineFast;
    mva SECOND_DST_LINE_ADDR ZP_FIRST_ADDR
    mva SECOND_DST_LINE_ADDR+1 ZP_FIRST_ADDR+1
    mva FIRST_DST_LINE_ADDR ZP_SECOND_ADDR
    mva FIRST_DST_LINE_ADDR+1 ZP_SECOND_ADDR+1
 
    jsr function_copy_line

    ;for y := 1 to IMAGE_HEIGHT do
    mva #1 y

loop

    ;if y = IMAGE_HEIGHT then
    ;begin
    ;ZP_FIRST_ADDR := SECOND_DST_LINE_ADDR;
    ;ZP_SECOND_ADDR := THIRD_DST_LINE_ADDR;
    ;CopyLineFast;
    ;end
    lda y
    cmp ROI_HEIGHT_ADDR
    bne not_last_row
    mva SECOND_DST_LINE_ADDR ZP_FIRST_ADDR
    mva SECOND_DST_LINE_ADDR+1 ZP_FIRST_ADDR+1
    mva THIRD_DST_LINE_ADDR ZP_SECOND_ADDR
    mva THIRD_DST_LINE_ADDR+1 ZP_SECOND_ADDR+1
    jsr function_copy_line
    jmp after_check
    
not_last_row

    ;else
    ;begin
    ;ZP_SECOND_ADDR := THIRD_DST_LINE_ADDR;
    ;ZP_FIRST_ADDR := TMP_FIRST_ADDR;
    ;Inc(ZP_FIRST_ADDR, IMAGE_STEP);
    ;TMP_FIRST_ADDR := ZP_FIRST_ADDR;
    ;LineSum111Fast
    ;end;
    mva THIRD_DST_LINE_ADDR ZP_SECOND_ADDR
    mva THIRD_DST_LINE_ADDR+1 ZP_SECOND_ADDR+1
    mva TMP_FIRST_ADDR ZP_FIRST_ADDR
    mva TMP_FIRST_ADDR+1 ZP_FIRST_ADDR+1

    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1
    
    mva ZP_FIRST_ADDR TMP_FIRST_ADDR
    mva ZP_FIRST_ADDR+1 TMP_FIRST_ADDR+1
    jsr function_line_sum_111
    
after_check

    ;ZP_FIRST_ADDR := Word(FIRST_DST_LINE_ADDR);
    ;ZP_SECOND_ADDR := Word(SECOND_DST_LINE_ADDR);
    ;ZP_THIRD_ADDR := Word(THIRD_DST_LINE_ADDR);
    mva FIRST_DST_LINE_ADDR ZP_FIRST_ADDR
    mva FIRST_DST_LINE_ADDR+1 ZP_FIRST_ADDR+1
    mva SECOND_DST_LINE_ADDR ZP_SECOND_ADDR
    mva SECOND_DST_LINE_ADDR+1 ZP_SECOND_ADDR+1
    mva THIRD_DST_LINE_ADDR ZP_THIRD_ADDR
    mva THIRD_DST_LINE_ADDR+1 ZP_THIRD_ADDR+1

    ;for x := 1 to IMAGE_STEP do
    ldy #0
    sty srcX
    sty dstX
    
internal_loop

    ;left := DIV_BY_9[(Peek(Word(firstPtr)) + Peek(Word(secondPtr)) +
    ;Peek(Word(thirdPtr)))];
    ldy srcX
    lda (ZP_FIRST_ADDR),y
    clc
    adc (ZP_SECOND_ADDR),y
    adc (ZP_THIRD_ADDR),y
    tay
    lda div_by_9,y
    tax

    ;Inc(firstPtr);
    ;Inc(secondPtr);
    ;Inc(thirdPtr);
    ldy srcX
    iny

    ;right := DIV_BY_9[(Peek(Word(firstPtr)) + Peek(Word(secondPtr)) +
    ;Peek(Word(thirdPtr)))];
    ;Inc(firstPtr);
    ;Inc(secondPtr);
    ;Inc(thirdPtr);
    lda (ZP_FIRST_ADDR),y
    clc
    adc (ZP_SECOND_ADDR),y
    adc (ZP_THIRD_ADDR),y
    iny
    sty srcX
    tay
    lda div_by_9,y
    sta right

    ;Poke(Word(ZP_FOURTH_ADDR), (left shl 4) or right);
    ;lda left
    txa
    asl
    asl
    asl
    asl
    ora right
    ldy dstX

    ;Inc(ZP_FOURTH_ADDR);
    sta (ZP_FOURTH_ADDR),y
    iny
    sty dstX
    cpy ROI_WIDTH_BYTES_ADDR
    bne internal_loop

    ;TMP_SECOND_ADDR := FIRST_DST_LINE_ADDR;
    ;FIRST_DST_LINE_ADDR := SECOND_DST_LINE_ADDR;
    ;SECOND_DST_LINE_ADDR := THIRD_DST_LINE_ADDR;
    ;THIRD_DST_LINE_ADDR := TMP_SECOND_ADDR;
    mva FIRST_DST_LINE_ADDR TMP_SECOND_ADDR
    mva FIRST_DST_LINE_ADDR+1 TMP_SECOND_ADDR+1
    mva SECOND_DST_LINE_ADDR FIRST_DST_LINE_ADDR
    mva SECOND_DST_LINE_ADDR+1 FIRST_DST_LINE_ADDR+1
    mva THIRD_DST_LINE_ADDR SECOND_DST_LINE_ADDR
    mva THIRD_DST_LINE_ADDR+1 SECOND_DST_LINE_ADDR+1
    mva TMP_SECOND_ADDR THIRD_DST_LINE_ADDR
    mva TMP_SECOND_ADDR+1 THIRD_DST_LINE_ADDR+1

    ;Inc(ZP_FOURTH_ADDR, IMAGE_STEP);
    lda ZP_FOURTH_ADDR
    add #IMAGE_STEP
    sta ZP_FOURTH_ADDR
    scc
    inc ZP_FOURTH_ADDR+1

    inc y
    lda y
    cmp ROI_HEIGHT_PLUS_ONE_ADDR
    beq end_loop
    jmp loop

end_loop

    plr
    rts

y dta 0
srcX equ $e8
dstX equ $e9
right equ $ea

.endl