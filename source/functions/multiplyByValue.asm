function_multiply_by_value

.local

    phr

    ldx #MAX_LUMINANCE

loop

    stx :ecx
    mva INTENSITY_VALUE_ADDR :eax
    imulCL
    lda :eax

    cmp #MAX_LUMINANCE+1
    bcc value_ok

    lda #MAX_LUMINANCE

value_ok

    sta LUT_ARRAY_ADDR,x

    dex
    bpl loop

    jsr function_lut

    plr
    rts

.endl