function_flip_vertical

.local

    phr

    ldy ROI_HEIGHT_ADDR
    cpy #1
    beq flip_vert_end

    ;ZP_FIRST_ADDR := Word(DST_PTR_ADDR);
    mva DST_PTR_ADDR ZP_FIRST_ADDR
    mva DST_PTR_ADDR+1 ZP_FIRST_ADDR+1

    ;ZP_SECOND_ADDR := ZP_FIRST_ADDR + IMAGE_STEP * roiHeight;
    mva #IMAGE_STEP :ecx
    mva #0 :ecx+1
    mva ROI_HEIGHT_ADDR :eax
    mva #0 :eax+1
    imulCX
    lda ZP_FIRST_ADDR
    add :eax
    sta ZP_SECOND_ADDR
    lda ZP_FIRST_ADDR+1
    adc :eax+1
    sta ZP_SECOND_ADDR+1

    ;for y := 1 to IMAGE_HALF_HEIGHT do
    lda ROI_HEIGHT_ADDR
    lsr
    sta y

loop

    ;Dec(ZP_SECOND_ADDR, IMAGE_STEP);
    lda ZP_SECOND_ADDR
    sub #IMAGE_STEP
    sta ZP_SECOND_ADDR
    scs
    dec ZP_SECOND_ADDR+1

    ;FlipLines
    ldy ROI_WIDTH_BYTES_ADDR
    dey
  
internal_loop

    lda (ZP_FIRST_ADDR),y
    tax
    lda (ZP_SECOND_ADDR),y
    sta (ZP_FIRST_ADDR),y
    txa
    sta (ZP_SECOND_ADDR),y
    dey

    bpl internal_loop

    ;Inc(ZP_FIRST_ADDR, IMAGE_STEP);
    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    dec y
    bne loop
  
flip_vert_end

    plr
    rts

y equ $e8

.endl