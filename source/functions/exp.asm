function_exp

.local

    phr

    ldy #MAX_LUMINANCE

loop

    mva exp_array,y LUT_ARRAY_ADDR,y
    dey
    bpl loop

    jsr function_lut

    plr
    rts

exp_array

    dta 1, 3, 7, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15

.endl