function_copy_line

.local

    ldy #IMAGE_WIDTH-1

loop

    lda (ZP_FIRST_ADDR),y
    sta (ZP_SECOND_ADDR),y
    dey
    bpl loop

    rts

.endl