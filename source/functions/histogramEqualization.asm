function_histogram_equalization

.local

    phr

    jsr function_calc_histogram

    lda ROI_ACTIVE_ADDR
    beq roi_not_active

    mva ROI_HEIGHT_ADDR :ecx
    mva ROI_WIDTH_ADDR :eax
    imulCL
    mva :eax pixels
    mva :eax+1 pixels+1

    jmp pixels_set

roi_not_active

    mva #.lo(IMAGE_PIXELS) pixels
    mva #.hi(IMAGE_PIXELS) pixels+1

pixels_set

    ldy #0
    sty sum
    sty sum+1

loop

    sty y

    ;Inc(sum, Histogram[i])
    lda sum
    add HISTOGRAM_LO_ARRAY_ADDR,y
    sta sum
    lda sum+1
    adc HISTOGRAM_HI_ARRAY_ADDR,y
    sta sum+1

    ;tmp := sum * MAX_LUMINANCE
    mva sum :ecx
    mva sum+1 :ecx+1
    mva #MAX_LUMINANCE :eax
    mva #0 :eax+1
    imulCX

    ;tmp := tmp div pixels
    inx
    mva :eax :STACKORIGIN,x
    mva :eax+1 :STACKORIGIN+STACKWIDTH,x
    mva :eax+2 :STACKORIGIN+STACKWIDTH*2,x
    mva :eax+3 :STACKORIGIN+STACKWIDTH*3,x
    inx
    mva pixels :STACKORIGIN,x
    mva pixels+1 :STACKORIGIN+STACKWIDTH,x
    mva #0 :STACKORIGIN+STACKWIDTH*2,x
    sta :STACKORIGIN+STACKWIDTH*3,x
    jsr idivCARD
    dex
    ldy y
    mva :eax LUT_ARRAY_ADDR,y
    dex

    iny
    cpy #MAX_LUMINANCE+1
    bne loop

    jsr function_lut

    plr
    rts

pixels equ $e8
sum equ $ea
y equ $ec

.endl