function_minimum_with_value

.local

    phr

    ldy #MAX_LUMINANCE

loop

    tya
    cmp INTENSITY_VALUE_ADDR
    bmi value_ok
    lda INTENSITY_VALUE_ADDR

value_ok

    sta LUT_ARRAY_ADDR,y

    dey
    bpl loop

    jsr function_lut

    plr
    rts

.endl