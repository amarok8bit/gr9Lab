function_sqr

.local

    phr

    ldy #MAX_LUMINANCE

loop

    mva sqr_array,y LUT_ARRAY_ADDR,y
    dey
    bpl loop

    jsr function_lut

    plr
    rts

sqr_array

    dta 0, 1, 4, 9, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15

.endl