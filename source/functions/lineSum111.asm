function_line_sum_111

.local

    ;value := Peek(Word(srcPtrLine));
    ;Inc(srcPtrLine);
    ldy #0
    lda (ZP_FIRST_ADDR),y
    tax
    iny
    sty srcX

    ;left := value shr 4;
    ;first := left;
    ;second := left;
    lsr
    lsr
    lsr
    lsr
    sta left
    sta first
    sta second

    ;right := value and $0f;
    ;third := right;
    txa
    and #$0F
    sta right
    sta third

    ;sum := first + second + third;
    lda first
    clc
    adc second
    adc third
    sta sum

    ;Poke(Word(dstPtrLine), sum);
    ;Inc(dstPtrLine, 1);
    ldy #0
    sta (ZP_SECOND_ADDR),y
    iny
    sty dstX

    ;for x := 1 to IMAGE_STEP_MINUS_1 do
    ldy srcX

loop

    ;value := Peek(Word(srcPtrLine));
    lda (ZP_FIRST_ADDR),y
    tax

    ;left := value shr 4;
    lsr
    lsr
    lsr
    lsr
    sta left

    ;sum := sum + left - first
    clc
    adc sum
    sec
    sbc first
    sta sum

    ;Poke(Word(dstPtrLine), sum);
    ;Inc(dstPtrLine);
    ldy dstX
    sta (ZP_SECOND_ADDR),y
    iny

    ;first := second;
    ;second := third;
    ;third := left;
    lda second
    sta first
    lda third
    sta second
    lda left
    sta third

    ;right := value and $0f;
    txa
    and #$0F
    sta right

    ;sum := sum + right - first;
    clc
    adc sum
    sec
    sbc first
    sta sum

    ;Poke(Word(dstPtrLine), sum);
    ;Inc(dstPtrLine);
    sta (ZP_SECOND_ADDR),y
    iny
    sty dstX

    ;first := second;
    ;second := third;
    ;third := right;
    lda second
    sta first
    lda third
    sta second
    lda right
    sta third

    ;Inc(srcPtrLine);
    ldy srcX
    iny
    sty srcX

    cpy ROI_WIDTH_BYTES_ADDR
    bne loop

    ;sum := sum - first + third;
    lda sum
    add third
    sub first
    sta sum

    ;Poke(Word(dstPtrLine), sum);
    ;Inc(dstPtrLine);
    ldy dstX
    sta (ZP_SECOND_ADDR),y

    rts

srcX equ $e8
dstX equ $e9
left equ $ea
right equ $eb
first equ $ec
second equ $ed
third equ $ee
sum equ $ef

.endl