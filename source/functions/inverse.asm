function_inverse

.local

    phr

    mva DST_PTR_ADDR ZP_FIRST_ADDR
    mva DST_PTR_ADDR+1 ZP_FIRST_ADDR+1

    ;for y := 1 to IMAGE_HEIGHT do
    mva ROI_HEIGHT_ADDR y

loop

    ;for x := 1 to IMAGE_STEP do
    ldy ROI_WIDTH_BYTES_ADDR
    dey

internal_loop

    ;Poke(Word(DST_PTR_ADDR), Peek(Word(DST_PTR_ADDR)) xor $ff);
    lda (ZP_FIRST_ADDR),y
    eor #$FF
    sta (ZP_FIRST_ADDR),y

    dey
    bpl internal_loop

    ;Inc(ZP_FIRST_ADDR, IMAGE_STEP);
    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    dec y
    bne loop

    plr
    rts

y equ $e8

.endl