function_emboss_filter

.local

    phr

    ;DST_PTR_ADDR := DST_PTR_ADDR;
    ;ZP_FIRST_ADDR := DST_PTR_ADDR;
    ;DST_PTR_ADDRLine := DST_PTR_ADDR;
    lda DST_PTR_ADDR
    sta ZP_FIRST_ADDR
    lda DST_PTR_ADDR+1
    sta ZP_FIRST_ADDR+1

    ;for y := 1 to IMAGE_HEIGHT do
    ;begin
    mva #1 y

loop

    ;value := Peek(Word(ZP_FIRST_ADDR));
    ;Inc(ZP_FIRST_ADDR);
    ldy #0
    lda (ZP_FIRST_ADDR),y
    tax
    iny

    ;left := value shr 4;
    ;first := left;
    ;second := left;
    lsr
    lsr
    lsr
    lsr
    sta left
    sta first
    sta second

    ;right := value and $0f;
    ;third := right;
    txa
    and #$0F
    sta right
    sta third

    ;dstLeft := MAX_LUMINANCE_DIV_2 + first - third;
    lda #MAX_LUMINANCE_DIV_2
    clc
    adc first
    sec
    sbc third

    ;if dstLeft < 0 then
    ;begin
    ;dstLeft := 0;
    ;end
    ;else if dstLeft > MAX_LUMINANCE then
    ;begin
    ;dstLeft := MAX_LUMINANCE;
    ;end;
    bcc to_low_1
    cmp #MAX_LUMINANCE
    bcs to_high_1
    jmp ready_1

to_low_1

    lda #0
    jmp ready_1

to_high_1

    lda #MAX_LUMINANCE

ready_1

    asl
    asl
    asl
    asl
    sta dstLeft 

    ;for x := 1 to IMAGE_STEP_MINUS_1 do
    ;begin

internal_loop
 
    ;value := Peek(Word(ZP_FIRST_ADDR));
    ;Inc(ZP_FIRST_ADDR);
    lda (ZP_FIRST_ADDR),y
    dey
    tax

    ;left := value shr 4;
    lsr
    lsr
    lsr
    lsr
    sta left

    ;first := second;
    ;second := third;
    ;third := left;
    mva second first
    mva third second
    mva left third

    ;dstRight := MAX_LUMINANCE_DIV_2 + first - third;
    lda #MAX_LUMINANCE_DIV_2
    clc
    adc first
    sec
    sbc third

    ;if dstRight < 0 then
    ;begin
    ;dstRight := 0;
    ;end
    ;else if dstRight > MAX_LUMINANCE then
    ;begin
    ;dstRight := MAX_LUMINANCE;
    ;end;
    bcc to_low_2
    cmp #MAX_LUMINANCE
    bcs to_high_2
    jmp ready_2

to_low_2

    lda #0
    jmp ready_2

to_high_2

    lda #MAX_LUMINANCE

ready_2

    ;sta dstRight 

    ;Poke(Word(ZP_SECOND_ADDR), (dstLeft shl 4) or dstRight);
    ;Inc(ZP_SECOND_ADDR);
    ;lda dstRight
    ora dstLeft
    sta (ZP_FIRST_ADDR),y
    iny

    ;first := second;
    ;second := third;
    ;right := value and $0f;
    ;third := right;
    mva second first
    mva third second
    txa
    and #$0F
    sta right
    sta third

    ;dstLeft := MAX_LUMINANCE_DIV_2 + first - third;
    lda #MAX_LUMINANCE_DIV_2
    clc
    adc first
    sec
    sbc third

    ;if dstLeft < 0 then
    ;begin
    ;dstLeft := 0;
    ;end
    ;else if dstLeft > MAX_LUMINANCE then
    ;begin
    ;dstLeft := MAX_LUMINANCE;
    ;end;
    bcc to_low_3
    cmp #MAX_LUMINANCE
    bcs to_high_3
    jmp ready_3

to_low_3

    lda #0
    jmp ready_3

to_high_3

    lda #MAX_LUMINANCE

ready_3

    asl
    asl
    asl
    asl
    sta dstLeft 

    ;Inc(srcPtrLine);
    ;end;
    iny

    cpy ROI_WIDTH_BYTES_ADDR
    bne internal_loop

    ;first := second;
    ;second := third;
    mva second first
    mva third second

    ;dstRight := MAX_LUMINANCE_DIV_2 + first - third;
    lda #MAX_LUMINANCE_DIV_2
    clc
    adc first
    sec
    sbc third

    ;if dstRight < 0 then
    ;begin
    ;dstRight := 0;
    ;end
    ;else if dstRight > MAX_LUMINANCE then
    ;begin
    ;dstRight := MAX_LUMINANCE;
    ;end;
    bcc to_low_4
    cmp #MAX_LUMINANCE
    bcs to_high_4
    jmp ready_4

to_low_4

    lda #0
    jmp ready_4

to_high_4

    lda #MAX_LUMINANCE

ready_4

    ;sta dstRight 

    ;Poke(Word(ZP_SECOND_ADDR), (dstLeft shl 4) or dstRight);
    ;Inc(ZP_SECOND_ADDR);
    ;lda dstRight
    ora dstLeft
    dey
    sta (ZP_FIRST_ADDR),y
    iny

    ;Inc(ZP_FIRST_ADDR, IMAGE_STEP);
    ;Inc(ZP_SECOND_ADDR, IMAGE_STEP);
    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    inc y
    lda y
    cmp ROI_HEIGHT_PLUS_ONE_ADDR
    beq end_loop
    jmp loop

end_loop

    plr
    rts

y dta 0
dstLeft equ $e8;
dstRight equ $e9;
left equ $ea;
right equ $eb;
first equ $ec;
second equ $ed;
third equ $ee;

.endl