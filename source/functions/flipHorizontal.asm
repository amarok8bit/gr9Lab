function_flip_horizontal

.local

    phr

    mva DST_PTR_ADDR ZP_FIRST_ADDR
    mva DST_PTR_ADDR+1 ZP_FIRST_ADDR+1

    mva #.lo(LINE_1_ADDR) ZP_SECOND_ADDR
    mva #.hi(LINE_1_ADDR) ZP_SECOND_ADDR+1

    ;for y := 1 to IMAGE_HEIGHT do
    mva ROI_HEIGHT_ADDR y

    lda ROI_WIDTH_BYTES_ADDR
    asl
    sta startDecodeDstX
    dec startDecodeDstX

    lda ROI_LEFT_MASK_ADDR
    ora ROI_RIGHT_MASK_ADDR

    cmp #$FF
    beq startEncodeSrcX_0

    cmp #$0F
    beq startEncodeSrcX_1

    mva #$FF startEncodeSrcX
    jmp loop

startEncodeSrcX_0

    mva #0 startEncodeSrcX
    jmp loop

startEncodeSrcX_1

    mva #1 startEncodeSrcX

loop

    ;for x := 1 to IMAGE_STEP do
    ldy ROI_WIDTH_BYTES_ADDR
    dey
    sty srcX

    mva startDecodeDstX dstX

decode_loop

    ;value := Peek(Word(srcPtrLine));
    lda (ZP_FIRST_ADDR),y
    tax

    ;right := value and $0f;
    ldy dstX
    and #$0F
    sta (ZP_SECOND_ADDR),y
    dey

    ;left := value shr 4;
    txa
    lsr
    lsr
    lsr
    lsr
    sta (ZP_SECOND_ADDR),y
    dey
    sty dstX

    ldy srcX
    dey
    sty srcX
    bpl decode_loop

    ;for x := 1 to IMAGE_STEP do
    ldy ROI_WIDTH_BYTES_ADDR
    dey
    sty dstX

    mva startEncodeSrcX srcX

encode_loop

    ;right
    ldy srcX
    lda (ZP_SECOND_ADDR),y
    tax
    iny

    ;left
    lda (ZP_SECOND_ADDR),y
    asl
    asl
    asl
    asl
    sta left
    iny
    sty srcX

    ;Poke(Word(ZP_FIRST_ADDR), (left shl 4) or right);
    ;lda left
    txa
    ora left

    ldy dstX
    sta (ZP_FIRST_ADDR),y
    dey
    sty dstX
    bpl encode_loop

    ;Inc(ZP_FIRST_ADDR, IMAGE_STEP);
    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    dec y
    bne loop
  
    plr
    rts

srcX equ $e8
dstX equ $e9
y equ $ea
left equ $eb
startDecodeDstX equ $ec
startEncodeSrcX equ $ed

.endl