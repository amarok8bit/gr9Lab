function_xor_with_value

.local

    phr

    ldy #MAX_LUMINANCE

loop

    tya
    eor INTENSITY_VALUE_ADDR
    sta LUT_ARRAY_ADDR,y

    dey
    bpl loop

    jsr function_lut

    plr
    rts

.endl