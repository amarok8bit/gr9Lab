function_and_with_value

.local

    phr

    ldy #MAX_LUMINANCE

loop

    tya
    and INTENSITY_VALUE_ADDR
    sta LUT_ARRAY_ADDR,y

    dey
    bpl loop

    jsr function_lut

    plr
    rts

.endl