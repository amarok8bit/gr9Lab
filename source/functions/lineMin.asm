function_line_min

.local

    ;value := Peek(Word(srcPtrLine));
    ;Inc(srcPtrLine);
    ldy #0
    lda (ZP_FIRST_ADDR),y
    tax
    iny
    sty srcX

    ;third := value and $0f;
    and #$0F
    sta third

    ;second := value shr 4;
    txa
    lsr
    lsr
    lsr
    lsr
    sta second

    ;if second < third then
    ;begin
    ;min := second;
    ;end
    ;else begin
    ;min := third;
    ;end;
    ;lda second
    cmp third
    bcc second_lower_than_third
    lda third

second_lower_than_third

    ;Poke(Word(dstPtrLine), min);
    ;Inc(dstPtrLine);
    ldy #0
    sta (ZP_SECOND_ADDR),y
    iny
    sty dstX

    ;for x := 1 to IMAGE_STEP_MINUS_1 do
    ;begin

loop

    ;value := Peek(Word(srcPtrLine));
    ;Inc(srcPtrLine);
    ldy srcX
    lda (ZP_FIRST_ADDR),y
    tax
    iny
    sty srcX

    ;right := value and $0f;
    and #$0F
    sta right

    ;left := value shr 4;
    txa
    lsr
    lsr
    lsr
    lsr
    sta left
 
    ;first := second;
    ;second := third;
    ;third := left;
    lda second
    sta first
    lda third
    sta second
    lda left
    sta third

    ;if second < first then
    ;begin
    ;min := second;
    ;end
    ;else begin
    ;min := first;
    ;end;
    lda second
    cmp first
    bcc left_second_lower_than_first
    lda first

left_second_lower_than_first

    ;if third < min then
    ;begin
    ;min := third;
    ;end;
    cmp third
    bcc left_ready
    lda third

left_ready

    ;Poke(Word(dstPtrLine), min);
    ;Inc(dstPtrLine);
    ldy dstX
    sta (ZP_SECOND_ADDR),y
    iny

    ;first := second;
    ;second := third;
    ;third := right;
    lda second
    sta first
    lda third
    sta second
    lda right
    sta third

    ;if second < first then
    ;begin
    ;min := second;
    ;end
    ;else begin
    ;min := first;
    ;end;
    lda second
    cmp first
    bcc right_second_lower_than_first
    lda first

right_second_lower_than_first

    ;if third < min then
    ;begin
    ;min := third;
    ;end;
    cmp third
    bcc right_ready
    lda third

right_ready

    ;Poke(Word(dstPtrLine), min);
    ;Inc(dstPtrLine);
    sta (ZP_SECOND_ADDR),y
    iny
    sty dstX

    ldy srcX
    cpy ROI_WIDTH_BYTES_ADDR
    bne loop

    ;if third < second then
    ;begin
    ;min := third;
    ;end
    ;else begin
    ;min := second;
    ;end;
    lda third
    cmp second
    bcc third_lower_than_second
    lda second

third_lower_than_second

    ;Poke(Word(dstPtrLine), min);
    ;Inc(dstPtrLine);
    ldy dstX
    sta (ZP_SECOND_ADDR),y
    iny
    sty dstX

    rts

srcX equ $e8
dstX equ $e9
left equ $ea
right equ $eb
first equ $ec
second equ $ed
third equ $ee

.endl