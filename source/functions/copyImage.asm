function_copy_image

.local

    phr

    mva SRC_PTR_ADDR ZP_FIRST_ADDR
    mva SRC_PTR_ADDR+1 ZP_FIRST_ADDR+1

    mva DST_PTR_ADDR ZP_SECOND_ADDR
    mva DST_PTR_ADDR+1 ZP_SECOND_ADDR+1

    ;for y := 1 to IMAGE_HEIGHT do
    mva ROI_HEIGHT_ADDR y

loop

    ;for x := 1 to IMAGE_STEP do
    ldy ROI_WIDTH_BYTES_ADDR
    dey

internal_loop

    mva (ZP_FIRST_ADDR),y (ZP_SECOND_ADDR),y
    dey
    bpl internal_loop

    ;Inc(ZP_FIRST_ADDR, IMAGE_STEP);
    lda ZP_FIRST_ADDR
    add #IMAGE_STEP
    sta ZP_FIRST_ADDR
    scc
    inc ZP_FIRST_ADDR+1

    ;Inc(ZP_SECOND_ADDR, IMAGE_STEP);
    lda ZP_SECOND_ADDR
    add #IMAGE_STEP
    sta ZP_SECOND_ADDR
    scc
    inc ZP_SECOND_ADDR+1

    dec y
    bne loop

    plr
    rts

y equ $e8

.endl