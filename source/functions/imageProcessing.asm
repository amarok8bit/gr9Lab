    icl 'common.inc'

image_processing

    phr

    lda FUNCTION_KIND_ADDR
    asl
    tay
    mva functions,y run_function+1
    mva functions+1,y run_function+2

run_function

    jsr function_set

    plr
    rts

functions

    dta a(function_copy_image)
    dta a(function_inverse)
    dta a(function_flip_horizontal)
    dta a(function_flip_vertical)
    dta a(function_rotate_180)
    dta a(function_lut)
    dta a(function_box_filter)
    dta a(function_gauss_filter)
    dta a(function_sharpen_filter)
    dta a(function_laplace_filter)
    dta a(function_max_filter)
    dta a(function_min_filter)
    dta a(function_opening_filter)
    dta a(function_closing_filter)
    dta a(function_emboss_filter)
    dta a(function_draw_roi)
    dta a(function_save_roi_buffers)
    dta a(function_restore_roi_buffers)
    dta a(function_calc_histogram)
    dta a(function_add_noise)
    dta a(function_brightness_contrast)
    dta a(function_histogram_equalization)
    dta a(function_add_value)
    dta a(function_subtract_value)
    dta a(function_difference_from_value)
    dta a(function_multiply_by_value)
    dta a(function_divide_by_value)
    dta a(function_or_with_value)
    dta a(function_and_with_value)
    dta a(function_xor_with_value)
    dta a(function_minimum_with_value)
    dta a(function_maximum_with_value)
    dta a(function_threshold)
    dta a(function_log)
    dta a(function_exp)
    dta a(function_sqr)
    dta a(function_sqrt)
    dta a(function_image_blender)
    dta a(function_set)

    icl '..\data\divBy4.asm'
    icl '..\data\divBy8.asm'
    icl '..\data\divBy9.asm'
    icl 'copyImage.asm'
    icl 'inverse.asm'
    icl 'flipHorizontal.asm'
    icl 'flipVertical.asm'
    icl 'rotate180.asm'
    icl 'lut.asm'
    icl 'copyLine.asm'
    icl 'lineSum111.asm'
    icl 'boxFilter.asm'
    icl 'lineSum121.asm'
    icl 'gaussFilter.asm'
    icl 'sharpenFilter.asm'
    icl 'laplaceFilter.asm'
    icl 'lineMax.asm'
    icl 'maxFilter.asm'
    icl 'lineMin.asm'
    icl 'minFilter.asm'
    icl 'openingFilter.asm'
    icl 'closingFilter.asm'
    icl 'embossFilter.asm'
    icl 'drawRoi.asm'
    icl 'saveRoiBuffers.asm'
    icl 'restoreRoiBuffers.asm'
    icl 'calcHistogram.asm'
    icl 'addNoise.asm'
    icl 'brightnessContrast.asm'
    icl 'histogramEqualization.asm'
    icl 'addValue.asm'
    icl 'subtractValue.asm'
    icl 'differenceFromValue.asm'
    icl 'multiplyByValue.asm'
    icl 'divideByValue.asm'
    icl 'orWithValue.asm'
    icl 'andWithValue.asm'
    icl 'xorWithValue.asm'
    icl 'minimumWithValue.asm'
    icl 'maximumWithValue.asm'
    icl 'threshold.asm'
    icl 'log.asm'
    icl 'exp.asm'
    icl 'sqr.asm'
    icl 'sqrt.asm'
    icl 'imageBlender.asm'
    icl 'setValue.asm'