unit gui;

interface

uses action;

var
  lastAction: TAction;
  lastDuration: Word;

procedure GuiInit;
procedure GuiDeInit;
procedure UpdateStatus;
procedure CheckCursorPos;
procedure UpdateImageIndex;
procedure ChangeMenu;

implementation

uses crt, memoryMap, caption, utils, imageRoi, cursor, keyboard, imgCore, menu, displayList;

var
  oldGprior: Byte;
  oldSDMCTL: Byte;
  oldSDLSTL: Byte;
  oldVbl: Pointer;
  oldDli: Pointer;
  priorValue: Byte;

procedure Dli; interrupt; assembler;
var
  PRIOR: Byte absolute $D01B;
asm
{
  pha
  lda priorValue
  cmp #$01
  beq equal_01
     
  lda #$01
  jmp set_prior

equal_01
  lda #$41

set_prior
  sta priorValue
  sta PRIOR
  pla
};
end;

procedure Vbl; interrupt; assembler;
asm
{
  mva #$01 priorValue
  jmp xitvbv
};
end;

procedure GuiInit;
begin
  InitKeyboard;

  intensityVal := 8;
  brightness := 16;
  contrast := 16;

  roiLeft := 0;
  roiLeftBackup := 0;
  roiTop := 0;
  roiTopBackup := 0;
  roiWidth := 80;
  roiWidthBackup := 80;
  roiHeight := 192;  
  roiHeightBackup := 192;
  UpdateRoiMask;
  roiActive := False;
  roiVisible := False;
  stickAction := saNone;

  oldSDMCTL := SDMCTL;
  SDMCTL := 0;

  oldSDLSTL := SDLSTL;

  HPOSP0 := 0;
  
  topLinesCount := 1;

  oldVbl := nil;
  GetIntVec(iVbl, oldVbl);
  SetIntVec(iVbl, @Vbl);

  oldDli := nil;
  GetIntVec(iDLI, oldDli);
  SetIntVec(iDli, @Dli);
  NMIEN := $C0;

  SDMCTL := oldSDMCTL;

  COLOR1 := $02;
  COLOR2 := $02;
  COLOR4 := $00;

  oldGprior := GPRIOR;
  GPRIOR := %00000000;
  SDLSTL := Word(DL_ADDR_1);

  SetUpPMGraphics;

  imageAddr := Pointer(IMAGE_ADDRESSES[imageIndex]);

  RollDownImage(imageAddr);
  HPOSP0 := cursorX;
  ChangeMenu;  
  CheckCursorPos;
  UpdateStatus;

  COLOR1 := $0B;
end;

procedure GuiDeInit;
begin
  menuKind := mkMainMenu;
  ChangeMenu;
  RollUpImage(imageAddr);

  SDMCTL := 0;
  Delay(50);
  NMIEN := 0;
  SetIntVec(iDli, oldDli);
  SetIntVec(iVbl, oldVbl);

  GPRIOR := oldGprior;
  SDMCTL := oldSDMCTL;
  SDLSTL := oldSDLSTL;
  GRACTL := 0;

  DeinitKeyboard;
end;

procedure UpdateStatus;
const
  ADDR_7 = TEXT_BUFFER_ADDR + 7;
  ADDR_9 = TEXT_BUFFER_ADDR + 9;
  ADDR_10 = TEXT_BUFFER_ADDR + 10;
  ADDR_11 = TEXT_BUFFER_ADDR + 11;
  ADDR_14 = TEXT_BUFFER_ADDR + 14;
  ADDR_15 = TEXT_BUFFER_ADDR + 15;
  ADDR_16 = TEXT_BUFFER_ADDR + 16;
  ADDR_17 = TEXT_BUFFER_ADDR + 17;
  ADDR_20 = TEXT_BUFFER_ADDR + 20;
  ADDR_21 = TEXT_BUFFER_ADDR + 21;
  ADDR_22 = TEXT_BUFFER_ADDR + 22;
  ADDR_23 = TEXT_BUFFER_ADDR + 23;
  ADDR_27 = TEXT_BUFFER_ADDR + 27;
  ADDR_28 = TEXT_BUFFER_ADDR + 28;
var
  caption: TCaption;
  pos, len: Byte;
  buffer: array[0..39] of Byte absolute TEXT_BUFFER_ADDR;
  sss: TString;
  textAddr: Word;
begin 
  textAddr := STATUS_ADDR;
  FillChar(Pointer(TEXT_BUFFER_ADDR), 40, ' '~);
  Move(Pointer(CAPTIONS[capImage]), Pointer(TEXT_BUFFER_ADDR), 5);
  
  byteValue := imageIndex;
  Inc(byteValue);
  textBufferPos := 6;
  PutByteToTextBuffer;
  Poke(ADDR_7, Byte(':'~));

  if lastAction <> actHome then
  begin
    caption := TCaption(ACTION_CAPTIONS[lastAction]);
    len := CAPTIONS_LENGTHS[caption];
    Move(Pointer(CAPTIONS[caption]), POINTER(ADDR_9), len);
    pos := 10;
    Inc(pos, len);

  	sss := IntToAnticString(lastDuration);
    Move(@sss[1], @buffer[pos], Byte(sss[0]));
    Inc(pos, Byte(sss[0]));
    Inc(pos);
	
    buffer[pos] := Byte('m'~);
    Inc(pos);
    buffer[pos] := Byte('s'~);
  end
  else if cursorArea = caImage then
  begin
    Poke(ADDR_9, Byte('X'~));
    Poke(ADDR_10, Byte('='~));
    byteValue := imageX;
    textBufferPos := 11;
    PutByteToTextBuffer;
  
    Poke(ADDR_14, Byte('Y'~));
    Poke(ADDR_15, Byte('='~));
    byteValue := imageY;
    textBufferPos := 16;
    PutByteToTextBuffer;
  
    POKE(ADDR_20, Byte('C'~));
    POKE(ADDR_21, Byte('='~));
    byteValue := pickedColor;
    textBufferPos := 22;
    PutByteToTextBuffer;
  end
  else if cursorArea <> caMenu then
  begin
    POKE(ADDR_9, Byte('R'~));
    POKE(ADDR_10, Byte('X'~));
    POKE(ADDR_11, Byte('='~));
    byteValue := roiLeft;
    textBufferPos := 12;
    PutByteToTextBuffer;

    POKE(ADDR_15, Byte('R'~));
    POKE(ADDR_16, Byte('Y'~));
    POKE(ADDR_17, Byte('='~));
    byteValue := roiTop;
    textBufferPos := 18;
    PutByteToTextBuffer;
  
    POKE(ADDR_22, Byte('W'~));
    POKE(ADDR_23, Byte('='~));
    byteValue := roiWidth;
    textBufferPos := 24;
    PutByteToTextBuffer;

    POKE(ADDR_27, Byte('H'~));
    POKE(ADDR_28, Byte('='~));
    byteValue := roiHeight;
    textBufferPos := 29;
    PutByteToTextBuffer;
  end;
  
  Move(Pointer(TEXT_BUFFER_ADDR), Pointer(textAddr), 40);
end;

procedure CheckCursorPos;
var
  oldCursorArea: TCursorArea;
begin
  oldCursorArea := cursorArea;
  GetImagePos(topLinesCount);
  if stickAction = saNone then
  begin
    if imagePosSuccess then
    begin
      GetRoiCursorArea
    end;
    GetMenuPos;
  end
  else begin
    CheckRoiAction;
    cursorArea := oldCursorArea;
  end;
  if cursorArea <> oldCursorArea then
  begin
    DrawCursor;
  end;
end;

procedure ShowMenu;
begin
  menuItemIndex := 0;
  topLinesCount := MENU_HEIGHT[menuKind];
  FillDisplayList(imageAddr);
  UpdateMenu;
  CheckCursorPos;  
  UpdateStatus;
end;

procedure ChangeMenu;
begin
  CopyMenuItems;
  ShowMenu;
end;

procedure UpdateImageIndex;
begin
  HideRoi;
  imageAddr := Pointer(IMAGE_ADDRESSES[imageIndex]);
  FillDisplayList(imageAddr);
  lastDuration := 0;
  CheckCursorPos;
  UpdateStatus;
end;

end.