unit cursor;

interface

type
  TStickAction = (
    saNone, saSizeLeft, saSizeRight, saSizeTop, saSizeBottom);
  TCursorArea = (
    caMenu, caImage, caLeftRoi, caRightRoi, caTopRoi, caBottomRoi);
  TTriggerState = (
    tsUp, tsDown, tsPressed, tsReleased);

const
  MIN_CURSOR_X = 45;
  MAX_CURSOR_X = 204;
  MIN_CURSOR_Y = 20;
  MAX_CURSOR_Y = 220;

  MAX_CURSOR_AREA: Byte = 5;
  STICK_ACTION: array[0..MAX_CURSOR_AREA] of Byte = (
    saNone, saNone, saSizeLeft, saSizeRight, saSizeTop, saSizeBottom);

  MAX_TRIGGER_STATE = 3;
  TRIGGER_UP_CHANGE_STATE: array[0..MAX_TRIGGER_STATE] of Byte = (
    tsUp, tsReleased, tsReleased, tsUp);
  TRIGGER_DOWN_CHANGE_STATE: array[0..MAX_TRIGGER_STATE] of Byte = (
    tsPressed, tsDown, tsDown, tsPressed);

var
  cursorMoved: Boolean;
  cursorX, cursorY: Byte;
  cursorArea: TCursorArea;
  triggerState: TTriggerState;
  imageX, imageY: Byte;
  stickAction: TStickAction;

procedure SetUpPMGraphics;
procedure DrawCursor;
procedure MoveCursor;
procedure GetRoiCursorArea;
procedure CursorDelay;

implementation

uses crt, memoryMap, imgCore, settings;

const
  CURSOR_HEIGHT = 10;
  CURSOR_ARROW: array[0..CURSOR_HEIGHT - 1] of Byte = (
    %00000000,
    %00000000,
    %00000000,
    %00010000,
    %00011000,
    %00011100,
    %00011110,
    %00011111,
    %00010100,
    %00000010);

  CURSOR_CROSS: array[0..CURSOR_HEIGHT - 1] of Byte = (
    %00010000,
    %00010000,
    %00010000,
    %01101100,
    %00010000,
    %00010000,
    %00010000,
    %00000000,
    %00000000,
    %00000000);

  CURSOR_SIZE_H: array[0..CURSOR_HEIGHT - 1] of Byte = (
    %00000000,
    %00101000,
    %01101100,
    %11111110,
    %01101100,
    %00101000,
    %00000000,
    %00000000,
    %00000000,
    %00000000);

  CURSOR_SIZE_V: array[0..CURSOR_HEIGHT - 1] of Byte = (
    %00010000,
    %00111000,
    %01111100,
    %00010000,
    %01111100,
    %00111000,
    %00010000,
    %00000000,
    %00000000,
    %00000000);

  CURSORS: array[0..MAX_CURSOR_AREA] of Pointer = (
    @CURSOR_ARROW, @CURSOR_CROSS, @CURSOR_SIZE_H, @CURSOR_SIZE_H, CURSOR_SIZE_V, CURSOR_SIZE_V);

var
  currStick, lastStick: Byte;
  cursorCounter: Byte;
  delayValue: Byte;

procedure SetUpPMGraphics;
begin
  FillChar(Pointer(PM_P0_ADDR), $100, 0);

  cursorX := 120;
  cursorY := 120;
  PMBASE := Hi(PM_ADDR);
  PCOLR0 := $84; // color of cursor
  SDMCTL := %00111110;
  GRACTL := %00000011; // turn on players and missiles
end;

procedure ClearCursorTopLine;
begin
  Poke(PM_P0_ADDR + cursorY, 0);
end;

procedure ClearCursorBottomLine;
begin
  Poke(PM_P0_ADDR + cursorY + CURSOR_HEIGHT - 1, 0);
end;

procedure DrawCursor;
var
  index: Byte;
  addr: Pointer;
begin
  index := cursorY;
  addr := Pointer(CURSORS[cursorArea]);
  Move(addr, Pointer(PM_P0_ADDR + index), CURSOR_HEIGHT);
end;

procedure CursorUp;
begin
  if cursorY > MIN_CURSOR_Y then
  begin
    ClearCursorBottomLine;
    Dec(cursorY);
    DrawCursor;
    cursorMoved := True;
  end;
end;

procedure CursorDown;
begin
  if cursorY < MAX_CURSOR_Y then
  begin
    ClearCursorTopLine;
    Inc(cursorY);
    DrawCursor;
    cursorMoved := True;
  end;
end;

procedure CursorLeft;
begin
  if cursorX > MIN_CURSOR_X then
  begin
    Dec(cursorX);
    HPOSP0 := cursorX;
    cursorMoved := True;
  end;
end;

procedure CursorRight;
begin
  if cursorX < MAX_CURSOR_X then
  begin
    Inc(cursorX);
    HPOSP0 := cursorX;
    cursorMoved := True;
  end;
end;

procedure MoveJoystickCursor;
var
  joy: Byte;
	stick0: Byte absolute $0278;
	stick1: Byte absolute $0279;

const
  JOY_U: Byte = %00000001;
  JOY_D: Byte = %00000010;
  JOY_L: Byte = %00000100;
  JOY_R: Byte = %00001000;

begin
  if controllerPortTwo then currStick := stick1
  else currStick := stick0;

  joy := not (currStick or %11110000);
  if JOY_U and joy <> 0 then
  begin
    CursorUp
  end
  else if JOY_D and joy <> 0 then
  begin
    CursorDown
  end;

  if JOY_L and joy <> 0 then
  begin
    CursorLeft
  end
  else if JOY_R and joy <> 0 then
  begin
    CursorRight
  end
end;

procedure MoveMouseCursor;
var
  PORTA: Byte absolute $D300;
  lastCode, lastXCode, lastYCode: Byte;
  code, xCode, yCode: Byte;
  counter: Byte;

const
  NONE = 0;
  BACK = 1;
  FORW = 2;

  CODES: array[0..15] of Byte = (
    NONE, // 00 -> 00
    BACK, // 00 -> 01
    FORW, // 00 -> 10
    NONE, // 00 -> 11
    FORW, // 01 -> 00
    NONE, // 01 -> 01
    NONE, // 01 -> 10
    BACK, // 01 -> 11
    BACK, // 10 -> 00
    NONE, // 10 -> 01
    NONE, // 10 -> 10
    FORW, // 10 -> 11
    NONE, // 11 -> 00
    FORW, // 11 -> 01
    BACK, // 11 -> 10
    NONE);// 11 -> 11

begin
  counter := 0;
  lastCode := PORTA;
  repeat
    code := PORTA;
    if code <> lastCode then Break;
    Inc(counter);
  until (counter = 255);

  if controllerPortTwo then
  begin
    code := code shr 4;
    lastCode := lastCode shr 4;
  end;

  if controllerKind = ckAtariMouse then
  begin
    lastXCode := (lastCode and %0011) shl 2;
    xCode := code and %0011;
  end
  else begin
    lastXCode := (lastCode and %1010) shr 1;
    xCode := code and %1010;  
  end;

  xCode := xCode or lastXCode;
  xCode := CODES[xCode];
  
  if xCode = BACK then
  begin
    CursorLeft;
  end
  else if xCode = FORW then
  begin
    CursorRight;
  end;
 
  if controllerKind = ckAtariMouse then
  begin
    lastYCode := lastCode and %1100;
    yCode := (code and %1100) shr 2;
  end
  else begin
    lastYCode := lastCode and %0101;
    yCode := (code and %0101) shl 1;
  end;

  yCode := yCode or lastYCode;
  yCode := CODES[yCode];

  if yCode = BACK then
  begin
    CursorUp;
  end
  else if yCode = FORW then
  begin
    CursorDown;
  end;
end;

procedure MoveTrakballCursor;
const
  X_MOVEMENT = %00000010;
  X_DIRECTION = %00000001;
  Y_MOVEMENT = %00001000;
  Y_DIRECTION = %00000100;
var
  PORTA: Byte absolute $D300;

begin
  currStick := PORTA;
  if controllerPortTwo then
  begin
    currStick := currStick shr 4;
  end;

  if currStick and X_MOVEMENT <> lastStick and X_MOVEMENT then
  begin
    if currStick and X_DIRECTION = 0 then
    begin
      CursorLeft;
    end
    else begin
      CursorRight;
    end;
  end;

  if currStick and Y_MOVEMENT <> lastStick and Y_MOVEMENT then
  begin
    if currStick and Y_DIRECTION = 0 then
    begin
      CursorUp;
    end
    else begin
      CursorDown;
    end;
  end;

  lastStick := currStick;
end;

procedure MoveCursor;
begin
  cursorMoved := False;
  if controllerKind = ckJoystick then
  begin
    MoveJoystickCursor;
  end
  else if controllerKind = ckTrakball then
  begin
    MoveTrakballCursor;
  end
  else begin
    MoveMouseCursor;
  end;
end;

procedure GetRoiCursorArea;
begin
  if not roiActive then Exit;
  if imageX < roiLeft then Exit;
  if imageX > roiRight then Exit;
  if imageY < roiTop then Exit;
  if imageY > roiBottom then Exit;
  
  if imageX = roiLeft then
  begin
    cursorArea := caLeftRoi
  end
  else if imageX = roiRight then
  begin
    cursorArea := caRightRoi
  end
  else if imageY = roiTop then
  begin
    cursorArea := caTopRoi
  end
  else if imageY = roiBottom then
  begin
    cursorArea := caBottomRoi
  end
end;

procedure JoystickCursorDelay;
begin
  if currStick <> lastStick then
  begin
    cursorCounter := 0;
  end;

  if cursorCounter > 40 then
  begin
    delayValue := 1;
  end
  else if cursorCounter > 0 then
  begin
    delayValue := 20;
    Inc(cursorCounter);
  end
  else begin
    delayValue := 255;
    Inc(cursorCounter);
  end;

  Delay(delayValue);

  lastStick := currStick;
end;

procedure CursorDelay;
begin
  if controllerKind = ckJoystick then
  begin
    JoystickCursorDelay;
  end;
end;

end.