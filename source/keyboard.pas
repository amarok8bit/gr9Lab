unit keyboard;

interface

var
  key: Char;

const
  KEY_DEF: array[0..191] of Byte = (
    $6C, $6A, $3B, $8A, $8B, $6B, $2B, $2A, $6F, $80, $70, $75, $9B, $69, $2D, $3D,
    $76, $80, $63, $8C, $8D, $62, $78, $7A, $34, $80, $33, $36, $1B, $35, $32, $31,
    $2C, $20, $2E, $6E, $80, $6D, $2F, $81, $72, $80, $65, $79, $7F, $74, $77, $71,
    $39, $80, $30, $37, $7E, $38, $3C, $3E, $66, $68, $64, $80, $82, $67, $73, $61,
    $4C, $4A, $3A, $8A, $8B, $4B, $5C, $5E, $4F, $80, $50, $55, $9B, $49, $5F, $7C,
    $56, $80, $43, $8C, $8D, $42, $58, $5A, $24, $80, $23, $26, $1B, $25, $22, $21,
    $5B, $20, $5D, $4E, $80, $4D, $3F, $81, $52, $80, $45, $59, $9F, $54, $57, $51,
    $28, $80, $29, $27, $9C, $40, $7D, $9D, $46, $48, $44, $80, $83, $47, $53, $41,
    $0C, $0A, $7B, $80, $80, $0B, $1E, $1F, $0F, $80, $10, $15, $9B, $09, $1C, $1D,
    $16, $80, $03, $89, $80, $02, $18, $1A, $80, $80, $85, $80, $1B, $80, $FD, $80,
    $00, $20, $60, $0E, $80, $0D, $80, $81, $12, $80, $05, $19, $9E, $14, $17, $11,
    $80, $80, $80, $80, $FE, $80, $7D, $FF, $06, $08, $04, $80, $84, $07, $13, $01);

procedure InitKeyboard;
procedure DeinitKeyboard;

implementation

var
  KEYDEF: Word absolute $0079;
  oldKeyDef: Word;

// Idea taken from https://github.com/ascrnet/HW-Detect
function IsAtari800: Boolean; assembler;
const
  OSROM2 = $FFF8;

asm
{
  pha
  lda OSROM2
  cmp #255
  beq atari800
  cmp #221
  beq atari800
  cmp #214
  beq atari800
  cmp #243
  beq atari800
  cmp #34
  beq atari800

  lda #False
  jmp endproc

atari800
  
  lda #True

endproc
  
  sta Result
  pla
};
end;

procedure InitKeyboard;
begin
  oldKeyDef := KEYDEF;
  if IsAtari800 then
  begin
    KEYDEF := Word(@KEY_DEF);
  end;
end;

procedure DeinitKeyboard;
begin
  KEYDEF := oldKeyDef;
end;

end.