unit imageRoi;

interface

uses memoryMap;

const
  MAX_IMAGE_COUNT: Byte = 2;
  IMAGE_ADDRESSES: array[0..MAX_IMAGE_COUNT - 1] of Word = (
    IMAGE_1_ADDR, IMAGE_2_ADDR);

var
  roiVisible: Boolean;
  imageAddr: Pointer;
  imageIndex: Byte;
  imagePosSuccess: Boolean;
  pickedColor: Byte;

procedure SetDstPtr;
procedure SetSrcDstPtr;
procedure SetSrcSrc2DstPtr;

procedure ManageRoi;
procedure UpdateRoiMask;
procedure HideRoi;
procedure ShowRoi;
procedure ShowHideRoi;
procedure CheckRoiAction;
procedure GetImagePos(topLinesCount: Byte);

implementation

uses sysUtils, imgCore, cursor;

const
  MIN_CURSOR_IMG_Y = MIN_CURSOR_Y + 9;

var
  roiLastTime: Cardinal;

procedure SetDstPtr;
begin
  dstPtr := Word(imageAddr);
  if roiActive then
  begin
    Inc(dstPtr, roiTop * IMAGE_STEP + roiLeft shr 1);
  end;
end;

procedure SetSrcDstPtr;
var
  offset: Word;
begin
  srcPtr := IMAGE_ADDRESSES[(imageIndex + 1) mod 2];
  dstPtr := IMAGE_ADDRESSES[imageIndex];

  if roiActive then
  begin
    offset := roiTop * IMAGE_STEP + roiLeft shr 1;
    Inc(srcPtr, offset);
    Inc(dstPtr, offset);
  end;
end;

procedure SetSrcSrc2DstPtr;
var
  offset: Word;
begin
  srcPtr := IMAGE_ADDRESSES[(imageIndex + 1) mod 2];
  src2Ptr := IMAGE_ADDRESSES[imageIndex];
  dstPtr := IMAGE_ADDRESSES[imageIndex];

  if roiActive then
  begin
    offset := roiTop * IMAGE_STEP + roiLeft shr 1;
    Inc(srcPtr, offset);
    Inc(src2Ptr, offset);
    Inc(dstPtr, offset);
  end;
end;

procedure ManageRoi;
const
  FUN_IMAGE_PROCESSING_ADDR = IMAGE_PROCESSING_ADDR;
var
  time: Cardinal;
begin
  if roiActive then
  begin
    time := GetTickCount;
    if Byte(time - roiLastTime) > 20 then
    begin
      roiVisible := not roiVisible;
      roiLastTime := time;
      SetDStPtr;
      functionKind := Ord(fkDrawRoi);
      asm { jsr FUN_IMAGE_PROCESSING_ADDR };
    end;    
  end;
end;

procedure UpdateRoiMask;
begin
  roiWidthBytes := roiWidth shr 1;
  roiHeightPlusOne := roiHeight + 1;
  roiRight := roiLeft + roiWidth - 1;
  roiBottom := roiTop + roiHeight - 1;
  if roiLeft and 1 = 0 then
  begin
    roiLeftMask := $F0;
  end
  else begin
    roiLeftMask := $0F;
  end;
  
  if (roiLeft + roiWidth) and 1 = 0 then
  begin
    roiRightMask := $0F;
  end
  else begin
    roiRightMask := $F0;
  end;

  if (roiLeftMask = $0F) or (roiRightMask = $F0) then
  begin
    Inc(roiWidthBytes);
  end;

  roiWidthBytesMinusOne := roiWidthBytes - 1;
end;

procedure HideRoi;
const
  FUN_IMAGE_PROCESSING_ADDR = IMAGE_PROCESSING_ADDR;
begin
  if roiActive and roiVisible then
  begin
    roiVisible := False;
    SetDstPtr;
    functionKind := Ord(fkDrawRoi);
    asm { jsr FUN_IMAGE_PROCESSING_ADDR };
  end;
end;

procedure ShowRoi;
const
  FUN_IMAGE_PROCESSING_ADDR = IMAGE_PROCESSING_ADDR;
begin
  if roiActive and not roiVisible then
  begin
    roiVisible := True;
    SetDstPtr;
    functionKind := Ord(fkDrawRoi);
    asm { jsr FUN_IMAGE_PROCESSING_ADDR };
  end;
end;

procedure ShowHideRoi;
begin
  if roiActive then
  begin
    HideRoi;
    roiActive := False;
    roiLeftBackup := roiLeft;
    roiTopBackup := roiTop;
    roiWidthBackup := roiWidth;
    roiHeightBackup := roiHeight;
    roiLeft := 0;
    roiTop := 0;
    roiWidth := 80;
    roiHeight := 192;
    UpdateRoiMask;
    roiActive := False;
  end
  else begin
    roiLeft := roiLeftBackup;
    roiTop := roiTopBackup;
    roiWidth := roiWidthBackup;
    roiHeight := roiHeightBackup;
    roiActive := True;
    UpdateRoiMask;
    ShowRoi;
  end;
end;

procedure CheckRoiAction;
begin
  if not imagePosSuccess then Exit;
  if stickAction = saNone then Exit;

  if stickAction = saSizeLeft then
  begin
    if imageX > roiRight then stickAction := saSizeRight
  end
  else if stickAction = saSizeRight then
  begin
    if imageX < roiLeft then stickAction := saSizeLeft
  end
  else if stickAction = saSizeTop then
  begin
    if imageY > roiBottom then stickAction := saSizeBottom
  end
  else if stickAction = saSizeBottom then
  begin
    if imageY < roiTop then stickAction := saSizeTop;
  end;

  HideRoi;

  if stickAction = saSizeLeft then
  begin
    roiWidth := roiRight - imageX + 1;
    roiLeft := imageX
  end
  else if stickAction = saSizeRight then
  begin
    roiWidth := imageX - roiLeft + 1
  end
  else if stickAction = saSizeTop then
  begin
    roiHeight := roiBottom - imageY + 1;
    roiTop := imageY
  end
  else if stickAction = saSizeBottom then
  begin
    roiHeight := imageY - roiTop + 1
  end;

  UpdateRoiMask;
  ShowRoi;
end;

procedure GetImagePos(topLinesCount: Byte);
var
  minCursorImgY: Byte;
begin
  minCursorImgY := MIN_CURSOR_IMG_Y;
  Inc(minCursorImgY, (topLinesCount - 1) shl 3);
  
  imagePosSuccess := False;
  if cursorX >= MIN_CURSOR_X then
    if cursorX <= MAX_CURSOR_X then
      if cursorY >= minCursorImgY then
        if cursorY <= MAX_CURSOR_Y then
        begin
          imagePosSuccess := True;
        end;
    
  if imagePosSuccess then
  begin
    imageX := (cursorX - MIN_CURSOR_X) shr 1;
    imageY := cursorY - MIN_CURSOR_IMG_Y;
    pickedColor := Peek(IMAGE_ADDRESSES[imageIndex] + imageY * IMAGE_STEP + imageX shr 1);
    if imageX and 1 = 0 then
    begin
      pickedColor := pickedColor shr 4;
    end
    else begin
      pickedColor := pickedColor and $0f;
    end;

    cursorArea := caImage;
  end
  else begin
    cursorArea := caMenu;
  end;
end;

initialization
  roiLastTime := GetTickCount;
end.