unit memoryMap;

interface

const
  CAPTIONS_COUNT = 70;

  CAPTIONS_ADDR = $3C40;
  CAPTIONS_LENGTHS_ADDR = CAPTIONS_ADDR + 2 * CAPTIONS_COUNT;

  IMAGE_1_ADDR = $4010;
  IMAGE_2_ADDR = $6010;

  PM_ADDR = $8000;
  PM_P0_ADDR = PM_ADDR + $400;

  DL_ADDR_1 = $8500; // max 223 bytes
  DL_ADDR_2 = $8600; // max 223 bytes

  TEXT_ADDR = $8700; // 10*40 characters
  STATUS_ADDR = TEXT_ADDR + 400; // 40 characters
  TEXT_BUFFER_ADDR = STATUS_ADDR + 40; // 40 characters

  TMP_BUFFER_ADDR = TEXT_BUFFER_ADDR + 40; // 40 bytes

var
  SDMCTL: Word absolute $022F; 
  SDLSTL: Word absolute $0230;
  GPRIOR: Byte absolute $026F;
  PCOLR0: Byte absolute $02C0;
  COLOR1: Byte absolute $02C5;
  COLOR2: Byte absolute $02C6;
  COLOR4: Byte absolute $02C8;

  HPOSP0: Byte absolute $D000;
  PAL_NTSC: Byte absolute $D014;
  COLPF1: Byte absolute $D017;
  PRIOR: Byte absolute $D01B;
  GRACTL: Byte absolute $D01D;
  PMBASE: Byte absolute $D407;
  NMIEN: Byte absolute $D40E;

implementation
end.