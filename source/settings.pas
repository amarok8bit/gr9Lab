unit settings;

interface

type
  TControllerKind = (
    ckJoystick, ckAtariMouse, ckAmigaMouse, ckTrakball);

var
  controllerKind: TControllerKind;
  controllerPortTwo: Boolean;
  fastProcessing: Boolean;

procedure LoadSettings;
procedure SaveSettings;

implementation

const
  CFG_FILE_NAME = 'D:GR9LAB.CFG';
  FILE_VERSION: Byte = 0;
  BUFFER_SIZE = 4;
var
  buf: array[0..BUFFER_SIZE - 1] of Byte;
  f: File;

procedure LoadSettings;
begin
  {$I-}
  Assign(f, CFG_FILE_NAME);
  Reset(f, 1);

  if IOResult >= 128 then Exit;

  BlockRead(f, buf, BUFFER_SIZE);
  if buf[0] = FILE_VERSION then
  begin
    controllerKind := TControllerKInd(buf[1]);
    controllerPortTwo := Boolean(buf[2]);
    fastProcessing := Boolean(buf[3]);
  end;

  Close(f);
  {$I+}  
end;

procedure SaveSettings;
begin
  {$I-}
  Assign(f, CFG_FILE_NAME);
  Rewrite(f, 1);

  if IOResult >= 128 then Exit;

  buf[0] := FILE_VERSION;
  buf[1] := Byte(controllerKind);
  buf[2] := Byte(controllerPortTwo);
  buf[3] := Byte(fastProcessing);
  BlockWrite(f, buf, BUFFER_SIZE);

  Close(f);
  {$I+}  
end;

end.