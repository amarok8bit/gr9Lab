uses crt, sysutils;

{$R 'image.rc'}
{$R 'data.rc'}
{$R 'functions.rc'}

uses imgCore, memoryMap, splashScreen, caption, action, menu, cursor, keyboard, gui, imageRoi, inputOutput, settings;

var
  requestExit: Boolean;

procedure ClearImage;
begin
  FillChar(Pointer(dstPtr), IMAGE_SIZE, 0);
end;

procedure NormalizeHistogram;
var
  i: Byte;
  maxHiValue, maxLoValue: Byte;
  nominator, denominator: Integer;
begin
  maxHiValue := 0;
  maxLoValue := 0;
  i := 0;
  while i <= MAX_LUMINANCE do
  begin
    if maxHiValue < HistogramHi[i] then
    begin
      maxHiValue := HistogramHi[i];
      maxLoValue := HistogramLo[i];
    end
    else if maxHiValue = HistogramHi[i] then
    begin
      if maxLoValue < HistogramLo[i] then
      begin
        maxLoValue := HistogramLo[i];
      end;
    end;
    Inc(i);
  end;

  denominator := Word(maxHiValue shl 8) + maxLoValue;

  i := 0;
  while i <= MAX_LUMINANCE do
  begin
    nominator := Word(HistogramHi[i] shl 8) + HistogramLo[i];
    nominator := nominator shl 5;
    nominator := nominator div denominator;
    HistogramNorm[i] := nominator;
    Inc(i);
  end;
end;

procedure CalcHistogram;
const
  FUN_IMAGE_PROCESSING_ADDR = IMAGE_PROCESSING_ADDR;
begin
  functionKind := Ord(fkCalcHistogram);
  asm { jsr FUN_IMAGE_PROCESSING_ADDR };
  NormalizeHistogram;
end;

procedure NextController;
begin
  if menuControllerKind = ckTrakball then
  begin
    menuControllerKind := ckJoystick
  end
  else begin
    menuControllerKind := menuControllerKind + 1;
  end;
  UpdateMenu;
end;

procedure NextControllerPort;
begin
  menuControllerPortTwo := not menuControllerPortTwo;
  UpdateMenu;
end;

procedure NextFastProcessing;
begin
  menuFastProcessing := not menuFastProcessing;
  UpdateMenu;
end; 

procedure TryChangeImage;
begin
  case key of
    '1': imageIndex := 0;
    '2': imageIndex := 1;
    else Exit;
  end;

  UpdateImageIndex;
end;

var
  actionToRun: TAction;
  functionToRun: TFunctionKind;

const
  ACTION_INFO: array[0..ACTION_COUNT - 1] of Byte = (
    mkHome, mkProcess, mkFilter, mkMath, mkBlender,
    mkNewQuery, lsaLoad, lsaSave, mkCloseQuery, fkCopyImage, 0, mkSettings,
    fkFlipHorizontal, fkFlipVertical, fkRotate180, mkBrightnessContrast, 0, fkHistogramEqualization, fkInverse, 
      fkThreshold, fkAddNoise,
    fkBoxFilter, fkGaussFilter, fkSharpenFilter, fkLaplaceFilter, fkEmbossFilter, fkMaxFilter, fkMinFilter, fkOpeningFilter, fkClosingFilter,
    fkSet, fkAddValue, fkSubtractValue, fkDifferenceFromValue, fkMultiplyByValue, fkDivideByValue, fkLog, fkExp, fkSqr, fkSqrt,
      fkInverse, fkOrWithValue, fkAndWithValue, fkXorWithValue, fkMinimumWithValue, fkMaximumWithValue,
    iboAdd, iboSubtract, iboDifference, iboMultiply, iboDivide,
      iboAverage, iboOr, iboAnd, iboXor, iboMinimum, iboMaximum,
    0, mkMainMenu,
    0, mkMainMenu,
    0, 0, 0, mkMainMenu,
    0, 0, 0, 0, 0, mkMainMenu,
    0, mkMainMenu,
    mkLoadSave,
    mkMainMenu,
    0, 0, 0, mkMainMenu);

procedure TryExecuteAction;
const
  FUN_IMAGE_PROCESSING_ADDR = IMAGE_PROCESSING_ADDR;
  PAL_TICK_IN_MS = 20;
var
  action: TAction;
  actionKind: TActionKind;
  actionInfo: Byte;
  startTime: Cardinal;
  oldSDMCTL: Byte;
begin
  action := TAction(menuItems[menuItemIndex]);
  actionKind := TActionKind(ACTION_KIND[action]);
  actionInfo := ACTION_INFO[action];

  case actionKind of
    akChangeMenu:
    begin
      menuKind := TMenuKind(actionInfo);
      ChangeMenu;
      Exit
    end;

    akChangeMenuLoadSave:
    begin
      loadSaveAction := TLoadSaveAction(actionInfo);
      menuKind := mkLoadSave;
      ChangeMenu;
      Exit;
    end;

    akChangeMenuSettings:
    begin
      menuControllerKind := controllerKind;
      menuControllerPortTwo := controllerPortTwo;
      menuFastProcessing := fastProcessing;
      menuKind := mkSettings;
      ChangeMenu;
      Exit;
    end;

    akAcceptSettings:
    begin
      controllerKind := menuControllerKind;
      controllerPortTwo := menuControllerPortTwo;
      fastProcessing := menuFastProcessing;
      menuKind := mkMainMenu;
      ChangeMenu;
      Exit;
    end;

    akChangeMenuIntensity:
    begin
      actionToRun := action;
      intensityCaption := TCaption(ACTION_CAPTIONS[action]);
      functionToRun := TFunctionKind(actionInfo);
      menuKind := mkIntensity;
      ChangeMenu;
      Exit;
    end;

    akChangeMenuBrightnessContrast:
    begin
      actionToRun := action;
      functionToRun := fkBrightnessContrast;
      menuKind := mkBrightnessContrast;
      ChangeMenu;
      Exit;
    end;

    akNew:
    begin
      HideRoi;
      dstPtr := Word(imageAddr);
      ClearImage;
      menuKind := mkMainMenu;
      ChangeMenu;
      Exit;
    end;

    akLoadSave:
    begin
      HideRoi;
      dstPtr := Word(imageAddr);
      LoadSaveImage;
      if lastIOResult < 128 then
      begin
        menuKind := mkMainMenu;
      end
      else begin
        menuKind := mkLoadSaveError;
      end;
      ChangeMenu;
      Exit;
    end;

    akExit:
    begin
      requestExit := True;
      Exit
    end;

    akNotImplemented:
    begin
      Exit
    end;

    akHistogram:
    begin
      SetDstPtr;
      HideRoi;
      CalcHistogram;
      menuKind := mkHistogram;
      ChangeMenu;
      Exit;
    end;

    akInPlace:
    begin
      SetDstPtr;
      actionToRun := action;
      functionToRun := TFunctionKind(actionInfo);
    end;

    akInPlaceParams:
    begin
      SetDstPtr;
    end;

    akOneSource:
    begin
      SetSrcDstPtr;
      actionToRun := action;
      functionToRun := TFunctionKind(actionInfo);
    end;
    
    akTwoSources:
    begin
      SetSrcSrc2DstPtr;
      actionToRun := action;
      functionToRun := fkImageBlender;
      imageBlenderOperation := actionInfo;
    end;

    akGui:
    begin
      case action of
        actIntensityDec: IntensityDec;
        actIntensityInc: IntensityInc;
        actBrightnessDec: BrightnessDec;
        actBrightnessInc: BrightnessInc;
        actContrastDec: ContrastDec;
        actContrastInc: ContrastInc;
        actShowHideSelection:
        begin
          ShowHideRoi;
          menuKind := mkMainMenu;
          ChangeMenu;
        end;
        actController: NextController;
        actControllerPort: NextControllerPort;
        actFastProcessing: NextFastProcessing;
      end;
      Exit;
    end;

    else Exit;
  end;

  menuKind := mkMainMenu;
  ChangeMenu;
  HideRoi;

  oldSDMCTL := SDMCTL;
  if fastProcessing then
  begin
    SDMCTL := 0;
  end;
  startTime := GetTickCount;

  functionKind := Ord(fkSaveRoiBuffers);
  asm { jsr FUN_IMAGE_PROCESSING_ADDR };

  functionKind := Ord(functionToRun);
  asm { jsr FUN_IMAGE_PROCESSING_ADDR };

  functionKind := Ord(fkRestoreRoiBuffers);
  asm { jsr FUN_IMAGE_PROCESSING_ADDR };

  lastDuration := Word((GetTickCount - startTime) * PAL_TICK_IN_MS);
  if PAL_NTSC = 15 then lastDuration := lastDuration * 5 div 6;

  SDMCTL := oldSDMCTL;
  
  lastAction := actionToRun;
  UpdateStatus;
  lastAction := actHome;
end;

function UpperCase(c: Byte): Byte;
begin
  Result := c;
  if (Result >= 97) and (Result <= 122) then
  begin
    Result := Result and %10111111;
  end;
end;

procedure TryExecuteMenu;
const
  KEY_ESC: Byte = 27;
  KEY_TAB: Byte = 127;
  KEY_TAB_SHIFT: Byte = 159;
  KEY_ENTER: Byte = 155;
var
  i: Byte;
  action: TAction;
  caption: TCaption;
  addr: Word;
  index: Byte;
  shortCut: Byte;

begin
  if key = Char(KEY_TAB) then
  begin
    newMenuItemIndex := menuItemIndex;
    Inc(newMenuItemIndex);
    if newMenuItemIndex = menuItemCount then
    begin
      newMenuItemIndex := 0
    end;
    UpdateMenuItemIndex;
  end
  else if key = Char(KEY_TAB_SHIFT) then
  begin
    newMenuItemIndex := menuItemIndex;
    if newMenuItemIndex = 0 then
    begin
      newMenuItemIndex := menuItemCount
    end;
    Dec(newMenuItemIndex);
    UpdateMenuItemIndex;
  end
  else if key = Char(KEY_ENTER) then
  begin
    TryExecuteAction
  end
  else if key = Char(KEY_ESC) then
  begin
    if menuKind <> mkMainMenu then
    begin
      menuKind := mkMainMenu;
      ChangeMenu;
    end;
  end
  else begin
    i := 0;
    while i < menuItemCount do
    begin
      action := TAction(menuItems[i]);
      index := ACTION_SHORTCUT_INDEX[action];
      if index <> 255 then
      begin
        caption := TCaption(ACTION_CAPTIONS[action]);
        addr := CAPTIONS[caption] + index;
        shortCut := UpperCase(Peek(addr));
        if UpperCase(Ord(key)) = shortCut then
        begin
          newMenuItemIndex := i;
          UpdateMenuItemIndex;
          TryExecuteAction;
          break;
        end;
      end;
      Inc(i);
    end;
  end;
end;

procedure CheckStickTrigger;
var
	strig0: Byte absolute $0284;
	strig1: Byte absolute $0285;
  strig: Byte;
  
begin
  if controllerPortTwo then
  begin
    strig := strig1;
  end
  else begin
    strig := strig0;
  end;

  if strig = 0 then
  begin
    triggerState := TTriggerState(TRIGGER_DOWN_CHANGE_STATE[triggerState]);
  end
  else begin
    triggerState := TTriggerState(TRIGGER_UP_CHANGE_STATE[triggerState]);
  end;

  if triggerState = tsPressed then
  begin
    if menuPosPosSuccess then
    begin
      TryExecuteAction;
    end
    else if imagePosSuccess then
    begin
      if menuKind <> mkMainMenu then
      begin
        menuKind := mkMainMenu;
        ChangeMenu;
      end
      else if roiActive then
      begin
        stickAction := TStickAction(STICK_ACTION[cursorArea])
      end;
    end;
  end
  else if triggerState = tsReleased then
  begin
    stickAction := saNone;
  end;
end;

procedure MainLoop;
begin
  requestExit := False;
  while not requestExit do
  begin
    ManageRoi;
    CheckStickTrigger;
    if KeyPressed then
    begin
      key := ReadKey;
      if menuKind = mkLoadSave then
      begin
        EditFilePath;
      end
      else begin
        TryChangeImage;
      end;
      TryExecuteMenu;
    end
    else
    begin
      MoveCursor;
      if cursorMoved then
      begin
        CheckCursorPos;
        UpdateStatus;
      end;
      CursorDelay;
    end;
  end;
end;

begin
  Randomize;
  LoadSettings;

  ShowSplashScreen;

  GuiInit;
  MainLoop;  
  GuiDeinit;

  SaveSettings;
end.
