unit action;

interface

uses caption;

type
  TAction = (
    actHome, actProcess, actFilter, actMath, actBlender,
    actNew, actLoad, actSave, actExit, actCopy, actShowHideSelection, actSettings,
    actFlipHorizontal, actFlipVertical, actRotate180, actBrightnessContrast, actHistogram, actHistogramEqualization, actInverse, 
      actThreshold, actAddNoise,
    actBoxFilter, actGaussianBlur, actSharpen, actLaplaceEdges, actEmboss, actDilateFilter, actErodeFilter, actOpeningFilter, actClosingFilter,
    actSet, actAdd, actSubtract, actDifference, actMultiply, actDivide, actLog, actExp, actSqr, actSqrt,
      actNot, actOr, actAnd, actXor, actMinimum, actMaximum,
    actAddCalculator, actSubtractCalculator, actDifferenceCalculator, actMultiplyCalculator, actDivideCalculator,
      actAverageCalculator, actOrCalculator, actAndCalculator, actXorCalculator, actMinCalculator, actMaxCalculator,
    actNewYes, actNewNo,
    actExitYes, actExitNo,
    actIntensityDec, actIntensityInc, actIntensityOk, actIntensityCancel,
    actBrightnessDec, actBrightnessInc, actContrastDec, actContrastInc, actBrightnessContrastOk, actBrightnessContrastCancel,
    actLoadSaveOk, actLoadSaveCancel,
    actLoadSaveErrorOk,
    actHistogramOk,
    actController, actControllerPort, actFastProcessing, actSettingsOk);

  TActionKind = (
    akChangeMenu,
    akChangeMenuSettings,
    akChangeMenuLoadSave,
    akChangeMenuIntensity,
    akChangeMenuBrightnessContrast,
    akAcceptSettings,
    akNotImplemented,
    akNew,
    akLoadSave,
    akExit,
    akHistogram,
    akInPlace,
    akInPlaceParams,
    akOneSource,
    akTwoSources,
    akGui);

const    
  ACTION_COUNT: Byte = 79;

  ACTION_KIND: array[0..ACTION_COUNT - 1] of Byte = (
    akChangeMenu, akChangeMenu, akChangeMenu, akChangeMenu, akChangeMenu,
    akChangeMenu, akChangeMenuLoadSave, akChangeMenuLoadSave, akChangeMenu, akOneSource, akGui, akChangeMenuSettings,
    akInPlace, akInPlace, akInPlace, akChangeMenuBrightnessContrast, akHistogram, akInPlace, akInPlace, akChangeMenuIntensity, akInPlace,
    akInPlace, akInPlace, akInPlace, akInPlace, akInPlace, akInPlace, akInPlace, akInPlace, akInPlace,
    akChangeMenuIntensity, akChangeMenuIntensity, akChangeMenuIntensity, akChangeMenuIntensity, akChangeMenuIntensity, akChangeMenuIntensity, akInPlace, akInPlace, akInPlace, akInPlace,
      akInPlace, akChangeMenuIntensity, akChangeMenuIntensity, akChangeMenuIntensity, akChangeMenuIntensity, akChangeMenuIntensity,
    akTwoSources, akTwoSources, akTwoSources, akTwoSources, akTwoSources, akTwoSources,
      akTwoSources, akTwoSources, akTwoSources, akTwoSources, akTwoSources,
    akNew, akChangeMenu,
    akExit, akChangeMenu,
    akGui, akGui, akInPlaceParams, akChangeMenu,
    akGui, akGui, akGui, akGui, akInPlaceParams, akChangeMenu,
    akLoadSave, akChangeMenu,
    akChangeMenu,
    akChangeMenu,
    akGui, akGui, akGui, akAcceptSettings);

  ACTION_CAPTIONS: array[0..ACTION_COUNT - 1] of Byte = (
    capGr9Lab, capProcess, capFilter, capMath, capBlender,
    capNew, capLoad, capSave, capExit, capCopy, capShowHideSelection, capSettings,
    capFlipHorizontal, capFlipVertical, capRotate180, capBrightnessContrast, capHistogram, capHistogramEqualization, capInverse,
      capThreshold, capAddNoise,
    capBoxFilter, capGaussianBlur, capSharpen, capLaplaceEdges, capEmboss, capDilateFilter, capErodeFilter, capOpeningFilter, capClosingFilter,
    capSet, capAdd, capSubtract, capDifference, capMultiply, capDivide, capLog, capExp, capSqr, capSqrt,
      capNot, capOr, capAnd, capXor, capMinimum, capMaximum,
    capAdd, capSubtract, capDifference, capMultiply, capDivide, capAverage, capOr, capAnd, capXor, capMinimum, capMaximum,
    capYes, capNo,
    capYes, capNo,
    capDec, capInc, capOk, capCancel,
    capDec, capInc, capDec, capInc, capOk, capCancel,
    capOk, capCancel,
    capOk,
    capOk,
    capJoystick, capOne, capOff, capOk);

  ACTION_SHORTCUT_INDEX: array[0..ACTION_COUNT - 1] of Byte = (
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 5, 2,
    0, 5, 0, 0, 0, 10, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 1, 0, 0,
    0, 0, 1, 2, 0, 2, 0, 0, 1, 3, 0, 0, 2, 2, 1, 2,
    0, 1, 2, 0, 2, 2, 0, 2, 2, 1, 2,
    0, 0,
    0, 0,
    255, 255, 0, 0,
    255, 255, 255, 255, 0, 0,
    255, 255,
    0,
    0,
    255, 255, 255, 0);

implementation
end.